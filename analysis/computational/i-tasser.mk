
.DELETE_ON_ERROR:

I-TASSER_BASEDIR ?=
I-TASSER_DIR ?= $(I-TASSER_BASEDIR)/5.1
I-TASSER_LIBDIR ?= $(I-TASSER_BASEDIR)/ITLIB

JAVA_HOME ?= /usr/java/latest

I-TASSER ?= $(I-TASSER_DIR)/I-TASSERmod/runI-TASSER.pl \
-pkgdir $(I-TASSER_DIR) \
-libdir $(I-TASSER_LIBDIR) \
-java_home $(JAVA_HOME)

FILE2HTML ?= $(I-TASSER_BASEDIR)/file2html/file2html.py

I-TASSERFLAGS = \
-LBS true \
-EC  true \
-GO  true \
-runstyle parallel


# Override this with a fasta file containing the actual inputs
SEQUENCE_FILE ?= inputs.fasta

.PHONY: default
default:
	@echo "Choose 'all' or '<locus_tag>/result.tar.bz2'"

all: ;

$(SEQUENCE_FILE): ;

i-tasser.mk: ;

%/result.tar.bz2: %/seq.fasta
	echo >> $*.log; date >> $*.log
	. $(I-TASSER_BASEDIR)/local/env.sh; \
	qsub \
	-V \
	-cwd \
	-N $* \
	-b yes \
	-sync yes \
	-j yes \
	-o $*.log \
	"$(I-TASSER) $(I-TASSERFLAGS) -seqname $* -datadir $(CURDIR)/$(@D) && $(FILE2HTML) $*"

%/seq.fasta: $(SEQUENCE_FILE)
	mkdir -p $*
	leaff -f $< -s $* > $*/seq.fasta

include proteins.d

proteins.d: $(SEQUENCE_FILE)
	{ echo -n "all: "; \
	grep '^>' $< \
	| tr -d '>' \
	| cut -f 1 -d ' ' \
	| sed 's|$$|/result.tar.bz2|' \
	| tr '\n' ' '; \
	} > $@
