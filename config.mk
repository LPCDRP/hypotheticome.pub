topsrcdir := $(dir $(abspath $(lastword $(MAKEFILE_LIST))))

ANNPATH ?= $(topsrcdir)/../Mtb-H37Rv-annotation
VPATH := $(topsrcdir)/res $(VPATH)

# Use proper delimiters
AWK = awk -v FS='	' -v OFS='	'

NPROC ?= $(shell nproc)

VERSION = $(shell git describe --always)

.DELETE_ON_ERROR:

.PHONY: help
help:
	@$(call usage,$(description),$(targets))

# Common utilities
include $(topsrcdir)/lib.mk
