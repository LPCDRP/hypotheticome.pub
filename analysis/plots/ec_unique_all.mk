SHELL := /bin/bash 

.DELETE_ON_ERROR:
.SECONDARY:

HYP_SET := /home/dgunasek/projects/hypotheticome.pub/res/tblist-hypotheticome.tsv
TUBERCULIST_ALL := /home/dgunasek/projects/hypotheticome.pub/res/tuberculist-EC-go-all-tblist-genes.tsv
MANNOTATION := /home/dgunasek/projects/Mtb-H37Rv-annotation/features
RESOURCES := /home/dgunasek/projects/hypotheticome.pub/res

# DEPENDENCIE: R package: VennDiagram

#.PHONY : clean

temp_files :  temp/hyp_list temp/unique_ec_mannotation temp/unique_ec_iTASSER temp/unique_ec_hypotheticome temp/uniprot_all_ec_genes temp/unique_ec_uniprot_all temp/unique_ec_uniprot_hyp \
	      temp/kegg_all_ec_genes temp/unique_ec_kegg_all temp/tblist_all_ec_genes temp/unique_ec_tblist_all temp/biocyc_all_ec_genes temp/biocyc_ec_raw temp/unique_ec_biocyc_all \
	      plot_mannotation rename_mannotation plot rename clean
scripts : scripts/processKeggEc.py scripts/ecUniqueAll.R

# Get all the genes in the hypotheticome list
temp/hyp_list: $(HYP_SET)
	mkdir -p temp
	cut -f1 $(HYP_SET) | sort | uniq > $@ || touch $@

temp/unique_ec_mannotation: $(MANNOTATION)
	grep 'EC number' $(MANNOTATION)/*.tbl | grep -v 'ncnote' | cut -f6 | sort | uniq > $@ || touch $@

temp/unique_ec_iTASSER: $(MANNOTATION)
	grep 'EC_number' $(MANNOTATION)/EC.tbl | cut -f6 | cut -d ' ' -f1 | sort | uniq > $@ || touch $@

temp/unique_ec_hypotheticome: $(temp_files)
	cat temp/unique_ec_iTASSER temp/unique_ec_mannotation | sort | uniq > $@ || touch $@

temp/uniprot_all_ec_genes: $(RESOURCES)
	grep '(EC' $(RESOURCES)/uniprot.tsv | cut -f1 | grep -v '/' | sort | uniq > $@
	cat <(grep '(EC' $(RESOURCES)/uniprot.tsv | cut -f1 | grep '/' | tr / '\n') >> $@ || touch $@

temp/unique_ec_uniprot_all: $(RESOURCES) $(temp_files)
	grep '(EC' $(RESOURCES)/uniprot.tsv | cut -f2 | cut -d '(' -f2 | grep 'EC ' | cut -d ')' -f1 | cut -d ' ' -f2 | sort | uniq > $@ || touch $@

temp/unique_ec_uniprot_hyp: $(RESOURCES) $(temp_files)
	comm -1 -2 temp/hyp_list <(cat temp/uniprot_all_ec_genes | sort | uniq) | while read -r line ; do grep $$line $(RESOURCES)/uniprot.tsv; done \
	| grep '(EC' | cut -f2 | cut -d '(' -f2 | grep 'EC ' | cut -d ')' -f1 | cut -d ' ' -f2 | sort | uniq > $@ || touch $@

temp/kegg_all_ec_genes: $(RESOURCES)
	awk '$$NF !~ /NA/ { print $$1 }' $(RESOURCES)/kegg_annotations > $@ || touch $@
	
temp/unique_ec_kegg_all: $(RESOURCES) $(temp_files)
	awk -F '\t' '$$NF !~ /NA/ { print $$3 }' $(RESOURCES)/kegg_annotations | sed -e "s/^\[EC://" | sed -e "s/\]//" | sed -e 's/;/\\n/g' | grep -v '[a-zA-Z]' | sort | uniq > $@ || touch $@

temp/tblist_all_ec_genes: $(RESOURCES)
	awk '$$2 ~ /^[0-9]/ { print $$1 }' $(RESOURCES)/tuberculist-EC-go-all-tblist-genes.tsv | grep -v '/' | sort | uniq > $@ || touch $@
	cat <(awk '$$2 !~ /./ { print $$1 }' $(RESOURCES)/tuberculist-EC-go-all-tblist-genes.tsv | grep '/' | tr / '\n') >> $@ || touch $@

temp/unique_ec_tblist_all:
	awk '$$2 ~ /^[0-9]/ { print $$0 }' $(RESOURCES)/tuberculist-EC-go-all-tblist-genes.tsv | cut -f2 | sort | uniq > $@ || touch $@

temp/biocyc_all_ec_genes: $(RESOURCES)
	awk '$$NF ~ /EC/ { print $$2 }' $(RESOURCES)/BioCyc_annotations.txt| grep 'Rv' | cut -d'"' -f2 | sort | uniq > $@ || touch $@

temp/biocyc_ec_raw: $(RESOURCES) $(temp_files)
	awk '$$NF ~ /EC/ { print $$0 }' $(RESOURCES)/BioCyc_annotations.txt | sed -e "s/EC-/#/g" |  sed -e "s/\/\//\n/g" | sed -e "s/\t/\n/g" | grep '#' | sed -e "s/#//g" | sed -e "s/^ //g" \
	| grep -v '[a-zA-Z]' > $@ || touch $@

temp/unique_ec_biocyc_all: $(temp_files) $(scripts)
	python scripts/processKeggEc.py > $@ || touch $@

plot_mannotation: $(scripts) $(temp_files)
	Rscript scripts/ecUniqueAllMannotation.R 

rename_mannotation:
	mv Rplots.pdf ec_uniqueness_all_mannotation.pdf


plot: $(scripts) $(temp_files)
	Rscript scripts/ecUniqueAll.R 

rename:
	mv Rplots.pdf ec_uniqueness_all.pdf

clean:
	rm -r temp/


