#Adds a newline to files that do not have newlines at the end, without adding an additional newline to the end of files that already have newlines to ensure proper counting of new products. 
sed -i -e '$a\' features/*.tbl
#Replaces pipes with backslahes in accordance with NCBI .tbl format, snd ensures no trailing or leading spaces follow the slash, also in in accordance with NCBI .tbl format
sed -i 's/|/\//g' features/*.tbl | sed -i 's/ \/ /\//g' features/*.tbl
#sed -i 's/PMID:    /PMID:/g' features/*.tbl
sed -i 's/PMID:   /PMID:/g' features/*.tbl
sed -i 's/PMID:  /PMID:/g' features/*.tbl
sed -i 's/PMID:/PMID: /g' features/*.tbl
