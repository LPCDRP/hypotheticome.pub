
@article{@IDENTIFIER@,
	title = {{PATRIC}: the {Comprehensive} {Bacterial} {Bioinformatics} {Resource} with a {Focus} on {Human} {Pathogenic} {Species} ▿},
	volume = {79},
	issn = {0019-9567},
	shorttitle = {{PATRIC}},
	url = {http://www.ncbi.nlm.nih.gov/pmc/articles/PMC3257917/},
	doi = {10.1128/IAI.00207-11},
	abstract = {Funded by the National Institute of Allergy and Infectious Diseases, the Pathosystems Resource Integration Center (PATRIC) is a genomics-centric relational database and bioinformatics resource designed to assist scientists in infectious-disease research. Specifically, PATRIC provides scientists with (i) a comprehensive bacterial genomics database, (ii) a plethora of associated data relevant to genomic analysis, and (iii) an extensive suite of computational tools and platforms for bioinformatics analysis. While the primary aim of PATRIC is to advance the knowledge underlying the biology of human pathogens, all publicly available genome-scale data for bacteria are compiled and continually updated, thereby enabling comparative analyses to reveal the basis for differences between infectious free-living and commensal species. Herein we summarize the major features available at PATRIC, dividing the resources into two major categories: (i) organisms, genomes, and comparative genomics and (ii) recurrent integration of community-derived associated data. Additionally, we present two experimental designs typical of bacterial genomics research and report on the execution of both projects using only PATRIC data and tools. These applications encompass a broad range of the data and analysis tools available, illustrating practical uses of PATRIC for the biologist. Finally, a summary of PATRIC's outreach activities, collaborative endeavors, and future research directions is provided.},
	number = {11},
	urldate = {2016-06-19},
	journal = {Infection and Immunity},
	author = {Gillespie, Joseph J. and Wattam, Alice R. and Cammer, Stephen A. and Gabbard, Joseph L. and Shukla, Maulik P. and Dalay, Oral and Driscoll, Timothy and Hix, Deborah and Mane, Shrinivasrao P. and Mao, Chunhong and Nordberg, Eric K. and Scott, Mark and Schulman, Julie R. and Snyder, Eric E. and Sullivan, Daniel E. and Wang, Chunxia and Warren, Andrew and Williams, Kelly P. and Xue, Tian and Seung Yoo, Hyun and Zhang, Chengdong and Zhang, Yan and Will, Rebecca and Kenyon, Ronald W. and Sobral, Bruno W.},
	month = nov,
	year = {2011},
	pmid = {21896772},
	pmcid = {PMC3257917},
	pages = {4286--4298}
}