#!/usr/bin/env python
# AUTHOR: Deepika Gunasekaran
# TITLE: Process iTASSER EC
# DESCRIPTION: This script takes as input the EC.tbl file from Mtb-H37Rv-annotation repository and extracts all Rv gene ids which have an annotated EC number
# NOTE: All paths in this script are hard-coded
# NOTE: This script is called in ec_presence_all.mk


input_file = open('/home/dgunasek/projects/Mtb-H37Rv-annotation/features/EC.tbl', 'r').readlines()
#print(len(input_file))
genes_with_ec = []
genes_without_ec = []
for i in range(0, len(input_file)-1):
	if 'EC_number' in input_file[i+1]:
		i_elements = input_file[i].split()
		if len(i_elements) == 0:
			continue
		elif i_elements[0] == 'locus_tag':
			genes_with_ec.append(i_elements[1])
	elif 'locus_tag' in input_file[i+1] and len(input_file[i]) > 1:
		i_elements = input_file[i].split()
		genes_without_ec.append(i_elements[1])
if 'locus_tag' in input_file[-1]:
	i_elements = input_file[i].split()
	genes_without_ec.append(i_elements[1])
#print(len(genes_without_ec))
#print(len(genes_with_ec))

#output_file = open('/home/dgunasek/projects/hypotheticome/analysis/plots/temp/iTASSER_with_ec', 'w')
for gene in genes_with_ec:
	#output_file.write(gene)
	print(gene)
#output_file.close
