#!/usr/bin/env python
import re
import argparse
from collections import defaultdict
import itertools

parser = argparse.ArgumentParser(prog="EC-map", description='Converts the EC annotations to products')
parser.add_argument('-m', '--multipletbl', required=False, action='store_false',
                    help="If specified, appends the EC product annotations to the Rv .tbl "
                         "files they correspond to. By default, they are written to a separate file (ECproducts.tbl)")

args = parser.parse_args()
mapRvEC = defaultdict(list)

ecdigitregex = re.compile("\d.\d{1,2}.\d{1,2}.\d{1,4}")
rvregex = re.compile("Rv\d{4}A?c?")

allecs = []
allqualifiers = []
products = []
morethan1EC = []
tblstr = ""


def extractecfromtbl():
    with open("../../../Mtb-H37Rv-annotation/features/EC.tbl", 'U') as infile:
        ectbl = infile.read()
    for line in ectbl.split('locus_tag'):
        if re.search(ecdigitregex, line) is not None:
            allecs = [line[i.start():i.end()] for i in re.finditer(ecdigitregex, line)]
            allqualifiers = [line[i.end() + 2:i.end() + 10] for i in re.finditer(ecdigitregex, line)]
            rvpos = re.search(rvregex, line)
            rvnumber = line[rvpos.start():rvpos.end()]
            with open("/grp/valafar/resources/ENZYME/EC_name.map", 'U') as map_file:
                ecMap = map_file.readlines()
                for EC in allecs:
                    EC_tab = EC + "\t" 
                    products = [allqualifiers[allecs.index(EC)] + " " + line.split('\t')[1].strip() for line in ecMap if
                                EC_tab in line]
                    mapRvEC[rvnumber].append(products)


def writetotbl():
    for Rv in mapRvEC:
        tblstr = "../../../Mtb-H37Rv-annotation/features/" + Rv + ".tbl"
        with open(tblstr, 'U') as tblfile:
            tbl = tblfile.readlines()
            productindex = next(tbl.index(line) for line in tbl if "product" in line)
        if tbl[productindex].lower() != ', '.join(mapRvEC[Rv])[-8:].lower():
            tbl[productindex] = tbl[productindex].replace("\n", "") + ', ' + ', '.join(mapRvEC[Rv]) + "\n"
        with open(tblstr, 'w') as tblfile:
            tblfile.writelines(tbl)
        if len(mapRvEC[Rv]) > 1:
            morethan1EC.append(Rv + "\t" + tbl[productindex])
        with open("../../../Mtb-H37Rv-annotation/features/morethan1EC.tsv", 'w') as newEC:
            newEC.writelines(morethan1EC)    


def writetofile():
    with open("../../../Mtb-H37Rv-annotation/features/ECproducts.tbl", 'w') as tbloutput:
        for Rv in mapRvEC:
            tblstr = "../../../Mtb-H37Rv-annotation/features/" + Rv + ".tbl"
            with open(tblstr, 'U') as tblfile:
                tbl = tblfile.readlines()
            productindex = next(tbl.index(line) for line in tbl if "product" in line)
            #if tbl[productindex].lower() != ', '.join(mapRvEC[Rv])[-8:].lower():
            mapRvECchained = list(itertools.chain(*mapRvEC[Rv]))
            tbl[productindex] = tbl[productindex].replace("\n", "") + ', ' + ', '.join(mapRvECchained) + "\n"
            tbloutput.writelines(tbl)
            if len(mapRvECchained) >= 2:
                morethan1EC.append(Rv + "\t" + tbl[productindex])
            with open("../../../Mtb-H37Rv-annotation/features/morethan1EC.tsv", 'w') as newEC:
                newEC.writelines(morethan1EC)
                    
if __name__ == "__main__":
    extractecfromtbl()
    if args.multipletbl:
        writetofile()
    else:
        writetotbl()
