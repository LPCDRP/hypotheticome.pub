#!/bin/bash
for tblfile in $(ls features/*.tbl); do
    for line in $tblfile; do
        if grep -qP "EC number\t"[0-9] $line; then
           sed -i 's/EC number/EC_number/g' $line
        fi
    done
done
