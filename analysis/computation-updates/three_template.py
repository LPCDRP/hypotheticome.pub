import csv
import os
from os import listdir
from os.path import isfile, join
import collections
from xml.dom import minidom
import commands


def three_template_extract():
    GROUPHOME = os.environ['GROUPHOME']
    template_file = [f for f in listdir('results/') if isfile(join('results/', f))]
    gene_dict = collections.OrderedDict()
    for gene_file in template_file:
        gene = gene_file.split('_')[0]
        with open('results/' + gene_file, 'rb') as csvfile:
            template_list = []
            reader = csv.reader(csvfile, delimiter='\t', quotechar='|')
            next(reader)
            for line in reader:
                template_list.append(line)
        try:
            sort_list =  sorted(template_list, key=lambda x: x[4], reverse=True)
            gene_dict[gene] = sort_list[0], sort_list[1], sort_list[2]
        except:
            #print 'No template for ' + gene
            pass
        
    print '\t'.join(['Rv number', 'C Score', 'Template' , 'TM-Score' , 'IDEN' , 'E.C. Number' , 'Geometric Mean', 'Macro Molecular Name', 'GO Term' ])
    for k, v, in gene_dict.iteritems():
        c_score_path = GROUPHOME+"/data/depot/hypotheticome/tuberculist/"+k+"/cscore"
        try: #try to see if the following keyword exist in the EC dictionary
            report = open(c_score_path, "U")
            for line in report:
                if line.startswith("model1"):
                    line1 = line.split()
                    c_score = line1[1]
        except:
            c_score = "NA"
        for template_list in v:

            template = template_list[0]
            #print template
            TM_score = template_list[1]
            Iden = template_list[2]
            EC = template_list[3]
            TMID = template_list[4]
            if template[-1].isdigit() == True:
                new_template = template[0:len(template)-2] + '.' + template[-2]
            else:
                new_template = template[0:len(template)-1] + '.' + template[-1]
            commands.getoutput("curl -s http://www.rcsb.org/pdb/rest/describeMol?structureId=" + new_template + " >intermediate.xml")
            #print "http://www.rcsb.org/pdb/rest/describeMol?structureId=" + new_template
            xmldoc = minidom.parse('intermediate.xml')
            macro_molecule = xmldoc.getElementsByTagName('macroMolecule')
            macro_molecule_name = []
            for i in macro_molecule:
                name =str(i.toxml()).split('"')[1]
                macro_molecule_name.append(name)
            macro_name = ', '.join(macro_molecule_name)

            #print macro_molecule_name
            commands.getoutput("curl -s http://www.rcsb.org/pdb/rest/goTerms?structureId=" + new_template + " >intermediate1.xml")
            xmldoc1 = minidom.parse('intermediate1.xml')
            term = xmldoc1.getElementsByTagName('term')
            GO_name = []
            try:
                for i in term:
                    GO = str(i.toxml()).split('id="')[1].split('"')[0]
                    GO_name.append(GO)
            except:
                pass
            GO_final = ', '.join(GO_name)
            #print GO_final
            print '\t'.join([k, c_score, template, TM_score, Iden, EC, TMID, macro_name, GO_final])

if __name__=='__main__':
    three_template_extract()
    
