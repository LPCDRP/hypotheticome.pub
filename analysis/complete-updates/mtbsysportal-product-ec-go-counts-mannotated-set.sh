#!/bin/bash
printf "\nNumber of genes analyzed (number of genes that were characterized by mannotation but not Uniprot): 118\n\n"
printf "In Mtb Network Portal:\n"
products=$(echo $(cut -f3 ../../res/mtbsysportalGOECproductmannotatedset.tsv | grep [a-z] | wc -l))
hypo=$(echo $(cut -f3 ../../res/mtbsysportalGOECproductmannotatedset.tsv | grep -e "hypothe" -e unknown -e "conserved protein" -e uncharacterized | wc -l))
printf "\nnumber of proteins with functional assignments: "
printf $((products-hypo))
printf "\n\nhypothetical proteins: "
printf $((118-products+hypo))
printf "\n\nproteins annotated with at least one go term: "
cut -f5,6 ../../res/mtbsysportalGOECproductmannotatedset.tsv | grep "GO:" | wc -l
printf "\nproteins annotated with ec numbers: "
cut -f4 ../../res/mtbsysportalGOECproductmannotatedset.tsv | grep "EC" | wc -l
printf "\n*****\n\n"

printf "In TubercuList:\n"
printf "\nproteins annotated with at least one go term: "
cut -f3 tuberculist-EC-go-mannotated-set.tsv | grep "GO:" | wc -l
printf "\nproteins annotated with ec numbers: "
cut -f2 tuberculist-EC-go-mannotated-set.tsv | grep [0-9.-] | wc -l
printf "\n"
