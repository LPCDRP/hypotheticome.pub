
#if(!require(devtools)) install.packages("devtools")
#devtools::install_github("kassambara/ggpubr")
#install.packages("ggpubr")
library(gridExtra)
#library(ggpubr)
library(ggplot2)
library(ggthemes)
EC_path <- read.csv("../../EC_pathways.tsv", header = FALSE, sep = "\t")

theme_set(theme_tufte(12, "bold"))
color <- scale_fill_manual(values = c("#332288", "#6699CC", "#88CCEE", "#44AA99", "#117733", "#999933", "#DDCC77", "#661100", "#CC6677", "#882255", "#AA4499"))
alpha <- scale_alpha_discrete(range = c(0.2,1))
colors <- c("#332288", "#6699CC", "#88CCEE", "#44AA99", "#117733", "#999933", "#DDCC77", "#661100", "#CC6677", "#882255", "#AA4499")
colors[8]

##### Read in data #####

EC_path_sub <- read.csv("../../EC_pathways_AMENDED.csv", header = TRUE, sep = "\t")


bar <- ggplot(EC_path_sub, aes(Pathway, position=Subsytem))
bar + geom_bar(aes(fill=Subsytem)) +
  coord_flip() +
  theme(legend.position = "top") +
   ggtitle("Pathway", fill='Subsystem')

##### Create Large subsystem Graphs #####

sb <- ggplot(EC_path_sub, aes(x=reorder(Subsytem,Subsytem,function(x)-length(x)), fill = Subsytem))
by_category <- sb + geom_bar() +
  #coord_flip() +
  theme(legend.position = "top") +
  labs(x = "Subsystem", fill='Pathway') +
  color
  #scale_fill_brewer(palette = "Set3")
by_category

##### pie chart #####

pie <- ggplot(EC_path_sub, aes(x=factor(1), fill=Subsytem))+
  geom_bar(width = 20)+
  coord_polar("y") + 
  color +
  theme(axis.title = element_blank(), axis.text = element_blank(), axis.ticks = element_blank(), legend.text = element_text(size = 16, face = "bold" )) +
  geom_text(aes(x=Subsytem, y=Subsytem), label = signif(percent_sub_counts[EC_path_sub$Subsytem], digits=2), size=5)
pie

legend.text = element_text(colour="blue", size = 16, face = "bold"

facto

library(scales)
pie + geom_text(aes(x = factor(1) + c(0, cumsum(value)[-length(value)]), label = percent_sub_counts[Subsytem], size=5)

##### Find max among all pathways to set as max in each plot for uniformity of scale ######

tbl <- table(EC_path_sub$Pathway)
path_max <- as.integer(max(tbl))
tbl

##### Make a new data frame for each subsystem category #####

for (sub in unique(EC_path_sub$Subsytem)){
  attach(EC_path_sub)
  assign(paste0(sub), data.frame(EC_path_sub[ which(Subsytem==sub),]))
  detach(EC_path_sub)
}

##### Make plots for each Subsystem #####

a <- ggplot(`Metabolism of other amino acids`, aes(x=reorder(Pathway,Pathway,function(x)-length(x)), fill=Subsytem, alpha=(..count..)))
Met_other_AA <- a + geom_bar(width = 0.85) + scale_fill_manual(values="#661100") +
  coord_flip(ylim = c(0, path_max)) +
  scale_y_continuous(breaks=c(4,8,12)) +
   #ggtitle("Metabolism of other amino acids") +
  #theme(axis.text = element_text(size = 12))
  theme(axis.title.y = element_blank(), axis.text = element_blank(), axis.ticks = element_blank(), axis.title.x=element_blank(), plot.title = element_text(hjust = 0.5), legend.position = "none") +
  scale_alpha_continuous()
Met_other_AA

a <- ggplot(`Glycan biosynthesis and metabolism`, aes(x=reorder(Pathway,Pathway,function(x)-length(x)),  fill=Subsytem, alpha=(..count..)))
Glyc_met <- a + geom_bar(width = 0.85) + scale_fill_manual(values="#117733") +
  coord_flip(ylim = c(0, path_max)) +
  scale_y_continuous(breaks=c(4,8,12)) +
   #ggtitle("Glycan biosynthesis and metabolism") + 
  #theme(axis.text = element_text(size = 12))
  theme(axis.title.y = element_blank(), axis.text = element_blank(), axis.ticks = element_blank(), axis.title.x=element_blank(), legend.position = "none") +
  scale_alpha_continuous()
Glyc_met

a <- ggplot(`Lipid metabolism`, aes(x=reorder(Pathway,Pathway,function(x)-length(x)), fill=Subsytem, alpha=(..count..)))
Lip_met <- a + geom_bar(width = 0.85) + scale_fill_manual(values="#999933") +
  coord_flip(ylim = c(0, path_max)) +
  scale_y_continuous(breaks=c(4,8,12)) +
   #ggtitle("Lipid metabolism") +
  #theme(axis.text = element_text(size = 12))
  theme(axis.title.y = element_blank(), axis.text = element_blank(), axis.ticks = element_blank(), axis.title.x=element_blank(), legend.position = "none") +
  scale_alpha_continuous()
Lip_met

a <- ggplot(`Amino acid metabolism`, aes(x=reorder(Pathway,Pathway,function(x)-length(x)), fill=Subsytem, alpha=(..count..)))
AA_met <- a + geom_bar(width = 0.85) + scale_fill_manual(values="#332288") +
  coord_flip(ylim = c(0, path_max)) +
  scale_y_continuous(breaks=c(4,8,12)) +
   #ggtitle("Amino acid metabolism") +
  #theme(axis.text = element_text(size = 12))
  theme(axis.title.y = element_blank(), axis.text = element_blank(), axis.ticks = element_blank(), axis.title.x=element_blank(), legend.position = "none") +
  scale_alpha_continuous()
  alpha +
  scale_fill_manual(values=colors[1])
AA_met

a <- ggplot(`Biosynthesis of other secondary metabolites`, aes(x=reorder(Pathway,Pathway,function(x)-length(x)),fill=Subsytem, alpha=(..count..)))
Sec_met_bs <- a + geom_bar(width = 0.85) + scale_fill_manual(values="#6699CC") +
  coord_flip(ylim = c(0, path_max)) +
  scale_y_continuous(breaks=c(4,8,12)) +
   #ggtitle("Biosynthesis of other secondary metabolites") +
  #theme(axis.text = element_text(size = 12))
  theme(axis.title.y = element_blank(), axis.text = element_blank(), axis.ticks = element_blank(), axis.title.x=element_blank(), legend.position = "none") +
  scale_alpha_continuous()
  alpha +
  scale_fill_manual(values=colors[2])
Sec_met_bs

a <- ggplot(`Carbohydrate metabolism`, aes(x=reorder(Pathway,Pathway,function(x)-length(x)), fill=Subsytem, alpha=(..count..)))
Carb_met <- a + geom_bar(width = 0.85) + scale_fill_manual(values="#88CCEE") +
  coord_flip(ylim = c(0, path_max)) +
  scale_y_continuous(breaks=c(4,8,12)) +
   #ggtitle("Carbohydrate metabolism") +
  #theme(axis.text = element_text(size = 12))
  theme(axis.title.y = element_blank(), axis.text = element_blank(), axis.ticks = element_blank(), axis.title.x=element_blank(), legend.position = "none") +
  scale_alpha_continuous()
  alpha +
  scale_fill_manual(values=colors[3])
Carb_met

a <- ggplot(`Energy metabolism`, aes(x=reorder(Pathway,Pathway,function(x)-length(x)), fill=Subsytem, alpha=(..count..)))
En_met <- a + geom_bar(width = 0.85) + scale_fill_manual(values="#44AA99") +
  coord_flip(ylim = c(0, path_max)) +
  scale_y_continuous(breaks=c(4,8,12)) +
   #ggtitle("Energy metabolism") + 
  #theme(axis.text = element_text(size = 12))
  theme(axis.title.y = element_blank(), axis.text = element_blank() , axis.ticks = element_blank(), axis.title.x=element_blank(), legend.position = "none") +
  scale_alpha_continuous()
  alpha +
  scale_fill_manual(values=colors[4])
En_met

a <- ggplot(`Metabolism of terpenoids and polyketides`, aes(x=reorder(Pathway,Pathway,function(x)-length(x)), fill=Subsytem, alpha=(..count..)))
Terp_met <- a + geom_bar(width = 0.85) + scale_fill_manual(values="#CC6677") +
  coord_flip(ylim = c(0, path_max)) +
  scale_y_continuous(breaks=c(4,8,12)) +
   #ggtitle("Metabolism of terpenoids and polyketides") +
  #theme(axis.text = element_text(size = 12))
  theme(axis.title.y = element_blank(), axis.text = element_blank(), axis.ticks = element_blank(), axis.title.x=element_blank(), legend.position = "none") +
  scale_alpha_continuous()
  alpha +
  scale_fill_manual(values=colors[9])
Terp_met

a <- ggplot(`Metabolism of cofactors and vitamins`, aes(x=reorder(Pathway,Pathway,function(x)-length(x)),  fill=Subsytem, alpha=(..count..)))
Cof_met <- a + geom_bar(width = 0.85) + scale_fill_manual(values="#DDCC77") +
  coord_flip(ylim = c(0, path_max)) +
  scale_y_continuous(breaks=c(4,8,12)) +
   #ggtitle("Metabolism of cofactors and vitamins") +
  #theme(axis.text = element_text(size = 12))
  theme(axis.title.y = element_blank(), axis.text = element_blank(),  axis.ticks = element_blank(),axis.title.x=element_blank(), legend.position = "none") +
  scale_alpha_continuous()
  alpha +
  scale_fill_manual(values=colors[7])
Cof_met

a <- ggplot(`Nucleotide metabolism`, aes(x=reorder(Pathway,Pathway,function(x)-length(x)), fill=Subsytem, alpha=(..count..)))
Nuc_met <- a + geom_bar(width = 0.85) + scale_fill_manual(values="#882255") +
  coord_flip(ylim = c(0, path_max)) +
  scale_y_continuous(breaks=c(4,8,12)) +
   #ggtitle("Nucleotide metabolism") +
  #theme(axis.text = element_text(size = 12))
  theme(axis.title.y = element_blank(), axis.text = element_blank(), axis.ticks = element_blank(), axis.title.x=element_blank(), plot.title = element_text(hjust = 0.5, vjust = 0.5), legend.position = "none") +
  scale_alpha_continuous()
  alpha +
  scale_fill_manual(values=colors[10])
Nuc_met

a <- ggplot(`Xenobiotics biodegradation and metabolism`, aes(x=reorder(Pathway,Pathway,function(x)-length(x)), fill=Subsytem, alpha=(..count..)))
Xen_met <- a + geom_bar(width = 0.85) + scale_fill_manual(values="#AA4499") +
  coord_flip(ylim = c(0, path_max)) +
  scale_y_continuous(breaks=c(4,8,12)) +
   #ggtitle("Xenobiotics biodegradation and metabolism") +
  #theme(axis.text = element_text(size = 12))
  theme(axis.title.y = element_blank(), axis.text.y = element_blank(), axis.title.x=element_blank(), axis.ticks.y = element_blank(),legend.position = "none") +
  scale_alpha_continuous()
  alpha +
  scale_fill_manual(values=colors[11])
Xen_met

##### Arrange plots into single page #####

grid.arrange(pie,
             arrangeGrob(AA_met, ncol=1, nrow=1),
             arrangeGrob(Sec_met_bs, ncol=1, nrow=1),
             arrangeGrob(Carb_met, ncol = 1, nrow = 1),
            arrangeGrob(En_met, ncol=1, nrow=1),
            arrangeGrob(Glyc_met,ncol=1, nrow=1), 
            arrangeGrob(Lip_met, ncol=1, nrow=1),
            arrangeGrob(Cof_met, ncol=1, nrow=1),
             arrangeGrob(Met_other_AA,ncol = 1, nrow = 1),  
            arrangeGrob(Terp_met, ncol=1, nrow=1),
            arrangeGrob(Nuc_met, ncol=1, nrow=1),
            arrangeGrob(Xen_met, ncol = 1, nrow = 1),
             nrow = 12, ncol= 1,
             heights = c(3,1.4,1.5,1.6,0.6,0.7,1.5,1.1,0.6,1.4,0.4,1.1))



