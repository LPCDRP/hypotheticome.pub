.ONESHELL:

TAXON = UP000001584

# Allow overriding the email address, but also make it so it doesn't
# get recursively expanded later (when the shell is different)
EMAIL ?= $(shell echo "`whoami`@`hostname`")
EMAIL := $(EMAIL)

all: uniprot.tsv

# Adapted from an example at http://www.uniprot.org/help/programmatic_access
SUMMARIES = $(addprefix uniprot., tab list gff orig.fasta)
.PHONY: downloads
downloads: $(SUMMARIES)
$(SUMMARIES): SHELL=perl
$(SUMMARIES): .SHELLFLAGS=-e
$(SUMMARIES):
	@use strict;
	use warnings;
	use LWP::UserAgent;
	use HTTP::Date;

	my $$taxon = "$(TAXON)"; # Taxonomy identifier of organism.
	my $$file = '$@';
	my ($$format) = "$$file" =~ /\.([^.]+)$$/;
	my $$query = "http://www.uniprot.org/uniprot/?query=proteome:$$taxon&format=$$format";
	my $$contact = '$(EMAIL)';
	my $$agent = LWP::UserAgent->new(agent => "libwww-perl $$contact");
	my $$response = $$agent->mirror($$query, $$file);

	if ($$response->is_success) {
	  my $$results = $$response->header('X-Total-Results');
	  my $$release = $$response->header('X-UniProt-Release');
	  my $$date = sprintf("%4d-%02d-%02d", HTTP::Date::parse_date($$response->header('Last-Modified')));
	  print "Downloaded $$results entries of UniProt release $$release ($$date) to file $$file\n";
	}
	elsif ($$response->code == HTTP::Status::RC_NOT_MODIFIED) {
	  print "Data for taxon $$taxon is up-to-date.\n";
	}
	else {
	  die 'Failed, got ' . $$response->status_line .
	    ' for ' . $$response->request->uri . "\n";
	}

uniprot.tsv: SHELL=perl
uniprot.tsv: .SHELLFLAGS=-e
uniprot.tsv: uniprot.tab
	use strict;
	use warnings;

	open(INPUT, "<", "$<") or die;
	open(OUTPUT, "|-", "sort > $@");

	<INPUT>; # Throw out header line
	while (<INPUT>) {
		chomp;
		my (
		$$accession,
		$$entry,
		$$status,
		$$description,
		$$genes,
		$$organism,
		$$length,
		) = split("\t",$$_);

		my @genes = ( $$genes =~ /(Rv[0-9]{4}[^\s]*)/g );
		foreach my $$gene (@genes) {
			print OUTPUT join("\t",($$gene,$$description,$$accession,$$status))."\n";
		}
	}

tuberculist.orig.fasta:
	wget http://tuberculist.epfl.ch/TubercuList_R27_FASTA.txt -O $@

tuberculist.fasta: tuberculist.orig.fasta
	sed 's/_/ /' $< > $@

uniprot.fasta: SHELL=perl
uniprot.fasta: .SHELLFLAGS=-We
uniprot.fasta: uniprot.orig.fasta uniprot.tsv
	my $$seq_file;
	{
		local $$/ = undef;
		open(SEQUENCE, "<", "$(word 1,$^)");
		binmode SEQUENCE;
		$$seq_file = <SEQUENCE>;
		close SEQUENCE;
	}

	open(MAPPING, "<", "$(word 2,$^)");
	open(OUTFILE, ">", "$@");

	my %locus_tag;

	while(<MAPPING>) {
		chomp;
		my ($$cur_locus_tag, undef, $$cur_uniprot_id) = split("\t");
		$$locus_tag{$$cur_uniprot_id} = $$cur_locus_tag;
	}

	foreach my $$uniprot_id (keys %locus_tag) {
		$$seq_file =~ s/>(.+\|$$uniprot_id\|.+)/>$$locus_tag{$$uniprot_id} $$1/;
	}

	print(OUTFILE "$$seq_file");

subset ?= hypotheticome

%.$(subset).fasta: SHELL=perl
%.$(subset).fasta: .SHELLFLAGS=-We
%.$(subset).fasta: %.fasta tblist-$(subset).tsv
	use Bio::SeqIO;

	open(GENELIST, "cut -f1 $(word 2,$^) |");

	my @genelist;
	while(<GENELIST>) {
		chomp;
		push(@genelist, $$_);
	}
	close(GENELIST);

	my $$proteome = Bio::SeqIO->new(
	  -file   => "$(word 1,$^)",
	  -format => "fasta",
	);

	my $$subset = Bio::SeqIO->new(
	  -file   => ">$@",
	  -format => "fasta",
	);

	while (my $$protein = $$proteome->next_seq()) {
		my $$seqid = $$protein->primary_id;
		if (grep(/$$seqid$$/,@genelist)) {
			$$subset->write_seq($$protein);
		}
	}
