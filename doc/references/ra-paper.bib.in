
@article{@IDENTIFIER@,
	title = {{SMRT} {Genome} {Assembly} {Corrects} {Reference} {Errors}, {Resolving} the {Genetic} {Basis} of {Virulence} in {Mycobacterium} tuberculosis},
	copyright = {© 2016, Published by Cold Spring Harbor Laboratory Press. This pre-print is available under a Creative Commons License (Attribution-NoDerivs 4.0 International), CC BY-ND 4.0, as described at http://creativecommons.org/licenses/by-nd/4.0/},
	url = {http://biorxiv.org/content/early/2016/07/21/064840},
	doi = {10.1101/064840},
	abstract = {The genetic basis of virulence in Mycobacterium tuberculosis has been investigated through genome comparisons of its virulent (H37Rv) and attenuated (H37Ra) sister strains. Such analysis, however, relies heavily on the accuracy of the sequences. While the H37Rv reference genome has had several corrections to date, that of H37Ra is unmodified since its original publication. Here, we report the assembly and finishing of the H37Ra genome from single-molecule, real-time (SMRT) sequencing. Our assembly reveals that the number of H37Ra-specific variants is less than half of what the Sanger-based H37Ra reference sequence indicates, undermining and, in some cases, invalidating the conclusions of several studies. PE\_PPE family genes, which are intractable to commonly-used sequencing platforms because of their repetitive and GC-rich nature, are overrepresented in the set of genes in which all reported H37Ra-specific variants are contradicted. We discuss how our results change the picture of virulence attenuation and the power of SMRT sequencing for producing high-quality reference genomes.},
	language = {en},
	urldate = {2016-09-13},
	journal = {bioRxiv},
	author = {Elghraoui, Afif and Modlin, Samuel J. and Valafar, Faramarz},
	month = jul,
	year = {2016},
	pages = {064840},
	file = {Full Text PDF:/home/smodlin/.mozilla/firefox/mnoneje6.default/zotero/storage/SKCNUU58/Elghraoui et al. - 2016 - SMRT Genome Assembly Corrects Reference Errors, Re.pdf:application/pdf;Snapshot:/home/smodlin/.mozilla/firefox/mnoneje6.default/zotero/storage/AXJ4BNRA/064840.html:text/html}
}