#!/bin/bash
for tblfile in $(ls features/*.tbl); do
    for line in $tblfile; do
	if grep -qP "inference\t" $line; then
            if !(grep -q "similar to DNA sequence" $line || grep -q "similar to AA sequence" $line || grep -q "similar to RNA sequence" $line || grep -q "similar to RNA sequence, mRNA" $line || grep -q "similar to RNA sequence, EST" $line || grep -q "similar to RNA sequence, other RNA"$line || grep -q "profile" $line || grep -q "nucleotide motif" $line || grep -q "protein motif" $line || grep -q "ab initio prediction" $line || grep -q "alignment" $line ); then
	       printf $tblfile
	    fi
        fi
    done
done


