
@article{@IDENTIFIER@,
	title = {Bioinformatics tools and databases for whole genome sequence analysis of {Mycobacterium} tuberculosis},
	volume = {45},
	issn = {1567-1348},
	url = {https://www.sciencedirect.com/science/article/pii/S1567134816303963},
	doi = {10.1016/j.meegid.2016.09.013},
	abstract = {Tuberculosis (TB) is an infectious disease of global public health importance caused by Mycobacterium tuberculosis complex (MTC) in which M. tuberculosis (Mtb) is the major causative agent. Recent advancements in genomic technologies such as next generation sequencing have enabled high throughput cost-effective generation of whole genome sequence information from Mtb clinical isolates, providing new insights into the evolution, genomic diversity and transmission of the Mtb bacteria, including molecular mechanisms of antibiotic resistance. The large volume of sequencing data generated however necessitated effective and efficient management, storage, analysis and visualization of the data and results through development of novel and customized bioinformatics software tools and databases. In this review, we aim to provide a comprehensive survey of the current freely available bioinformatics software tools and publicly accessible databases for genomic analysis of Mtb for identifying disease transmission in molecular epidemiology and in rapid determination of the antibiotic profiles of clinical isolates for prompt and optimal patient treatment.},
	urldate = {2017-02-02},
	journal = {Infection, Genetics and Evolution},
	author = {Faksri, Kiatichai and Tan, Jun Hao and Chaiprasert, Angkana and Teo, Yik-Ying and Ong, Rick Twee-Hee},
	month = nov,
	year = {2016},
	keywords = {Analysis tools, Bioinformatics, Databases, Next generation sequencing, Tuberculosis, whole genome sequencing},
	pages = {359--368},
	file = {ScienceDirect Full Text PDF:/home/smodlin/.mozilla/firefox/mnoneje6.default/zotero/storage/HR37NWE6/Faksri et al. - 2016 - Bioinformatics tools and databases for whole genom.pdf:application/pdf;ScienceDirect Snapshot:/home/smodlin/.mozilla/firefox/mnoneje6.default/zotero/storage/NU2KV9KH/S1567134816303963.html:text/html}
}