import re
import os
import commands
import argparse
import sys
import collections
import csv

def ref_test():
    Ref_test = []
    with open("../../res/Ref_test_Rv_list.tsv", 'U') as filehandler:
        for line in filehandler:
            line = line.strip()
            Ref_test.append(line)
        return Ref_test

def molecular(Complete_Rv_list, MF_exp):
    Ref_test = []
    GROUPHOME= os.environ['GROUPHOME'] #set GROUHOME environment variable
    MF_dict = collections.OrderedDict() #create a dictionary
    MF_exp_dict = {}
    with open(MF_exp, 'U') as csvreader:
        filehandler = csv.reader(csvreader, delimiter = '\t')
        for line in filehandler:
            #print line
            MF_exp_dict[line[0]] = line[1:len(line)]

    with open(Complete_Rv_list, 'U') as Rv_list: #open the list
        for RV in Rv_list:
            MF_list = [] #set a list for this RV for the value in dictionary, reset everytime when loop through RV
            RV = RV.strip()
            MF_data = GROUPHOME+"/data/depot/hypotheticome/tuberculist/" + RV + "/model1/coach/GO_MF.dat" #load the data
            MF_data_ref = GROUPHOME + "/data/depot/hypotheticome/reftest/"+ RV + "/model1/coach/GO_MF.dat" #load the data
            if RV not in Ref_test:
                try:
                    with open(MF_data, 'U') as molecular_function: #try in case some genes doesn't have GO_MF.dat (because still running I-TASSER)
                        for line in molecular_function: #line for each GO_MF.dat
                            line = line.strip() #strip white space
                            line = line[0:10] + "," + line[11:15] + "," + line[16:] #since the line contain three things, we want GO term, it's score, and the function, so we add ',' to make able to split
                            line = line.split(",") #split with ','
                            MF_list.append(line) #append the line list to list
                            MF_dict[RV] = MF_list #add the dictionary of key = RV, value = the line list
                except:
                    pass
            else:
                try:
                    with open(MF_data_ref, 'U') as molecular_function: #try in case some genes doesn't have GO_MF.dat (because still running I-TASSER)
                        for line in molecular_function: #line for each GO_MF.dat
                            line = line.strip() #strip white space
                            line = line[0:10] + "," + line[11:15] + "," + line[16:] #since the line contain three things, we want GO term, it's score, and the function, so we add ',' to make able to split
                            line = line.split(",") #split with ','
                            MF_list.append(line) #append the line list to list
                            MF_dict[RV] = MF_list #add the dictionary of key = RV, value = the line list
                except:
                    pass

    with open('Molecular_function_GO.tsv', 'w') as output:
        output.write("Rv Number" +  '\t' + "GO number" + '\t' + 'C-score' + '\t' + 'Description' + '\t' + 'Match/Mismatch' + '\n')
        for k,v in MF_dict.iteritems(): #access key and value of this dictionary, it is in order because of OrderedDict
            for i in range(0, len(v)): #there are different number of values, so want to get all of it
                line = [k, v[i][0], v[i][1], v[i][2]] #we know there is 3 parts for each value
                if k in MF_exp_dict:
                    if v[i][0] in MF_exp_dict[k]:
                        line.append('1')
                    else:
                        line.append('0')
                else:
                    line.append('NA')
                output.write('\t'.join(line)+ '\n')
            
def cellular(Complete_Rv_list, CC_exp):
    Ref_test = []
    GROUPHOME= os.environ['GROUPHOME'] #set GROUHOME environment variable
    CC_dict = collections.OrderedDict() #create a dictionary
    CC_exp_dict = {}
    with open(CC_exp, 'U') as csvreader:
        filehandler = csv.reader(csvreader, delimiter = '\t')
        for line in filehandler:
            #print line
            CC_exp_dict[line[0]] = line[1:len(line)]

    with open(Complete_Rv_list, 'U') as Rv_list: #open the list
        for RV in Rv_list:
            CC_list = [] #set a list for this RV for the value in dictionary, reset everytime when loop through RV
            RV = RV.strip()
            CC_data = GROUPHOME+"/data/depot/hypotheticome/tuberculist/" + RV + "/model1/coach/GO_CC.dat" #load the data
            CC_data_ref = GROUPHOME + "/data/depot/hypotheticome/reftest/"+ RV + "/model1/coach/GO_CC.dat" #load the data
            if RV not in Ref_test:
                try:
                    with open(CC_data, 'U') as cellular_component: #try in case some genes doesn't have GO_CC.dat (because still running I-TASSER)
                        for line in cellular_component: #line for each GO_CC.dat
                            line = line.strip() #strip white space
                            line = line[0:10] + "," + line[11:15] + "," + line[16:] #since the line contain three things, we want GO term, it's score, and the function, so we add ',' to make able to split
                            line = line.split(",") #split with ','
                            CC_list.append(line) #append the line list to list
                            CC_dict[RV] = CC_list #add the dictionary of key = RV, value = the line list
                except:
                    pass
            else:
                try:
                    with open(CC_data_ref, 'U') as cellular_component: #try in case some genes doesn't have GO_CC.dat (because still running I-TASSER)
                        for line in cellular_component: #line for each GO_CC.dat
                            line = line.strip() #strip white space
                            line = line[0:10] + "," + line[11:15] + "," + line[16:] #since the line contain three things, we want GO term, it's score, and the function, so we add ',' to make able to split
                            line = line.split(",") #split with ','
                            CC_list.append(line) #append the line list to list
                            CC_dict[RV] = CC_list #add the dictionary of key = RV, value = the line list
                except:
                    pass

    with open('Cellular_component_GO.tsv', 'w') as output:
        output.write("Rv Number" +  '\t' + "GO number" + '\t' + 'C-score' + '\t' + 'Description' + '\t' + 'Match/Mismatch' + '\n')
        for k,v in CC_dict.iteritems(): #access key and value of this dictionary, it is in order because of OrderedDict
            for i in range(0, len(v)): #there are different number of values, so want to get all of it
                line = [k, v[i][0], v[i][1], v[i][2]] #we know there is 3 parts for each value
                if k in CC_exp_dict:
                    if v[i][0] in CC_exp_dict[k]:
                        line.append('1')
                    else:
                        line.append('0')
                else:
                    line.append('NA')
                output.write('\t'.join(line)+ '\n')

def biological(Complete_Rv_list, BP_exp):
    Ref_test = []
    GROUPHOME= os.environ['GROUPHOME'] #set GROUHOME environment variable
    BP_dict = collections.OrderedDict() #create a dictionary
    BP_exp_dict = {}
    with open(BP_exp, 'U') as csvreader:
        filehandler = csv.reader(csvreader, delimiter = '\t')
        for line in filehandler:
            BP_exp_dict[line[0]] = line[1:len(line)]

    with open(Complete_Rv_list, 'U') as Rv_list: #open the list
        for RV in Rv_list:
            BP_list = [] #set a list for this RV for the value in dictionary, reset everytime when loop through RV
            RV = RV.strip()
            BP_data = GROUPHOME+"/data/depot/hypotheticome/tuberculist/" + RV + "/model1/coach/GO_BP.dat" #load the data
            BP_data_ref = GROUPHOME + "/data/depot/hypotheticome/reftest/"+ RV + "/model1/coach/GO_BP.dat" #load the data
            if RV not in Ref_test:
                try:
                    with open(BP_data, 'U') as biological_process: #try in case some genes doesn't have GO_BP.dat (because still running I-TASSER)
                        for line in biological_process: #line for each GO_BP.dat
                            line = line.strip() #strip white space
                            line = line[0:10] + "," + line[11:15] + "," + line[16:] #since the line contain three things, we want GO term, it's score, and the function, so we add ',' to make able to split
                            line = line.split(",") #split with ','
                            BP_list.append(line) #append the line list to list
                            BP_dict[RV] = BP_list #add the dictionary of key = RV, value = the line list
                except:
                    pass
            else:
                try:
                    with open(BP_data_ref, 'U') as biological_process: #try in case some genes doesn't have GO_BP.dat (because still running I-TASSER)
                        for line in biological_process: #line for each GO_BP.dat
                            line = line.strip() #strip white space
                            line = line[0:10] + "," + line[11:15] + "," + line[16:] #since the line contain three things, we want GO term, it's score, and the function, so we add ',' to make able to split
                            line = line.split(",") #split with ','
                            BP_list.append(line) #append the line list to list
                            BP_dict[RV] = BP_list #add the dictionary of key = RV, value = the line list
                except:
                    pass

    with open('Biological_process_GO.tsv', 'w') as output:
        output.write("Rv Number" +  '\t' + "GO number" + '\t' + 'C-score' + '\t' + 'Description' + '\t' + 'Match/Mismatch' + '\n')
        for k,v in BP_dict.iteritems(): #access key and value of this dictionary, it is in order because of OrderedDict
            for i in range(0, len(v)): #there are different number of values, so want to get all of it
                line = [k, v[i][0], v[i][1], v[i][2]] #we know there is 3 parts for each value
                if k in BP_exp_dict:
                    if v[i][0] in BP_exp_dict[k]:
                        line.append('1')
                    else:
                        line.append('0')
                else:
                    line.append('NA')
                output.write('\t'.join(line)+ '\n')

def cat():
    with open('Molecular_function_GO.tsv', 'U') as MF:
        for line in MF:
            line = line.strip()
            if line[2] == " ":
                line += '\t' + 'Function'
            else:
                line += '\t' + 'MF'
            print line
    with open('Biological_process_GO.tsv', 'U') as BP:
        next(BP)
        for line in BP:
            line= line.strip()
            line  += '\t' + 'BP'
            print line
    with open('Cellular_component_GO.tsv', 'U') as CC:
        next(CC)
        for line in CC:
            line= line.strip()
            line  += '\t' + 'CC'
            print line

def main(Complete_Rv_list,MF_exp, CC_exp, BP_exp):
    molecular(Complete_Rv_list, MF_exp)
    cellular(Complete_Rv_list, CC_exp)
    biological(Complete_Rv_list, BP_exp)
    
if __name__ == "__main__":
    main(sys.argv[1], sys.argv[2], sys.argv[3], sys.argv[4])
