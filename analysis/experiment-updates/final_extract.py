import csv
import sys


def RepresentsFloat(s):
    try:
        float(s)
        return True
    except ValueError:
        return False
    
def extract(f1):
    with open(f1, "U") as csvfile:
        reader = csv.reader(csvfile, delimiter='\t',quotechar='|',quoting=csv.QUOTE_MINIMAL) #able to read csv
        for row in reader:
            if RepresentsFloat(row[3]) == True:
                Iden= float(row[3])
                C_score = float(row[4])
                if C_score >= -1.5 and Iden <= 0.4:
                    print "\t".join(row)
            else:
                print "\t".join(row)

if __name__ == "__main__":
    extract(sys.argv[1])
