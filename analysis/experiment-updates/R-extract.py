import csv
import sys
import math


def r_extract(f1):
    with open(f1, 'rb') as csvfile:
        reader = csv.reader(csvfile, delimiter='\t',quotechar='|',quoting=csv.QUOTE_MINIMAL) #able to read csv
        for row in reader:
            if row[0] == "RV number":
                print '\t'.join([row[0], row[2], row[3], row[4], row[5],  "position", "Hit/No-Hit", 'TMID'])
            else:
                TMID = math.sqrt(float(row[2])*float(row[3]))
                TMID = str(TMID)
                line1 = '\t'.join([row[0], row[2], row[3], row[4], row[5],"1", row[10], TMID])
                line2 = '\t'.join([row[0], row[2], row[3], row[4], row[5],"2", row[11], TMID])
                line3 = '\t'.join([row[0], row[2], row[3], row[4], row[5],"3", row[12], TMID])
                line4 = '\t'.join([row[0], row[2], row[3], row[4], row[5],"4", row[13], TMID])
                print '\n'.join([line1, line2, line3, line4])
if __name__ == "__main__":
    r_extract(sys.argv[1])
        
    
