#!/usr/bin/env python
import urllib2
import sys
import re

fout = sys.stdout
mannotated = sys.stdin.readlines()
#fout = open('mtbsysportalgoterms.tsv', 'w')
#fin = open('/home/azlotnic/hypotheticome/hypotheticome/res/mtb-genes-entire-genome.tsv ', 'U')
#mannotated = fin.readlines()
gostartregex = re.compile('amigo\/term_details\?term=')
#goendregex = re.compile('<\/a><br \/><href\=http\:\/\/ami')
#fout.write("gene\tEC Number\tGO Terms") 
fout.writelines("gene\tproduct\tEC Number\tGO Terms") 
for x in range(0, len(mannotated)):
    url = 'http://tuberculist.epfl.ch/quicksearch.php?gene+name=' + mannotated[x].strip() + '&submit=Search'
    response = urllib2.urlopen(url)
    page = response.read(200000)
    #productstr = page[page.find('title>'):page.find('/title')] 
    #product = productstr[productstr.find(mannotated[x].strip()):productstr.find('|')]
    #product = product.replace(mannotated[x].strip(), "\t") 
    EC = page[page.find('.org/enzyme/'):page.find('</a></td></TR><TR><td width=160><b>Gene Ontology')]
    EC = EC[12:EC.find('\'>')]
    gostart = []
    goend = []
    gotermname = []
    gostart = [m.end() for m in re.finditer(gostartregex, page)]
    #goend = [n.start() for n in re.finditer(goendregex, page)]
    for i in range (0, len(gostart)):
        gotermname.append(page[gostart[i]:(gostart[i]+10)])
    fout.writelines("\n" + mannotated[x].strip() + '\t' + EC)
    for j in gotermname: 
        fout.writelines('\t' + j)
fout.close()
