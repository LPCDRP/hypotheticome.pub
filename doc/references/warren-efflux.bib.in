
@article{@IDENTIFIER@,
	title = {Energy {Metabolism} and {Drug} {Efflux} in {Mycobacterium} tuberculosis},
	volume = {58},
	issn = {0066-4804, 1098-6596},
	url = {http://aac.asm.org/content/58/5/2491},
	doi = {10.1128/AAC.02293-13},
	abstract = {The inherent drug susceptibility of microorganisms is determined by multiple factors, including growth state, the rate of drug diffusion into and out of the cell, and the intrinsic vulnerability of drug targets with regard to the corresponding antimicrobial agent. Mycobacterium tuberculosis, the causative agent of tuberculosis (TB), remains a significant source of global morbidity and mortality, further exacerbated by its ability to readily evolve drug resistance. It is well accepted that drug resistance in M. tuberculosis is driven by the acquisition of chromosomal mutations in genes encoding drug targets/promoter regions; however, a comprehensive description of the molecular mechanisms that fuel drug resistance in the clinical setting is currently lacking. In this context, there is a growing body of evidence suggesting that active extrusion of drugs from the cell is critical for drug tolerance. M. tuberculosis encodes representatives of a diverse range of multidrug transporters, many of which are dependent on the proton motive force (PMF) or the availability of ATP. This suggests that energy metabolism and ATP production through the PMF, which is established by the electron transport chain (ETC), are critical in determining the drug susceptibility of M. tuberculosis. In this review, we detail advances in the study of the mycobacterial ETC and highlight drugs that target various components of the ETC. We provide an overview of some of the efflux pumps present in M. tuberculosis and their association, if any, with drug transport and concomitant effects on drug resistance. The implications of inhibiting drug extrusion, through the use of efflux pump inhibitors, are also discussed.},
	language = {en},
	number = {5},
	urldate = {2017-10-04},
	journal = {Antimicrobial Agents and Chemotherapy},
	author = {Black, Philippa A. and Warren, Robin M. and Louw, Gail E. and Helden, Paul D. van and Victor, Thomas C. and Kana, Bavesh D.},
	month = may,
	year = {2014},
	pmid = {24614376},
	pages = {2491--2503},
	file = {Full Text PDF:/home/smodlin/.mozilla/firefox/mnoneje6.default/zotero/storage/PXQG7FEM/Black et al. - 2014 - Energy Metabolism and Drug Efflux in Mycobacterium.pdf:application/pdf;Snapshot:/home/smodlin/.mozilla/firefox/mnoneje6.default/zotero/storage/GUF4PD8G/2491.html:text/html}
}