#!/bin/bash
grep -vi "antigen" devils-advocate.html | grep -B 1 "#FFFF00" | sed 's/<td align="left" bgcolor="#FFFF00">//g'| sed 's/<\/td>//g' | sed 's/<td height="17" align="left">//g' > missing-no-antigens.txt
printf "\nannotation missing in other databases, excluding antigens:\n"
grep -Eio "Rv"[0-9]{4}[ABcD]? missing-no-antigens.txt | wc -l
grep -vi "antigen" devils-advocate.html | grep -B 1 "#FF00FF" | sed 's/<td align="left" bgcolor="#FF00FF">//g'| sed 's/<\/td>//g' | sed 's/<td height="17" align="left">//g' > stronger-no-antigens.txt
printf "\nannotation stronger than that in other databases, excluding antigens:\n"
grep -Eio "Rv"[0-9]{4}[ABcD]? stronger-no-antigens.txt | wc -l
grep -vi "antigen" devils-advocate.html | grep -B 1 "#00FFFF" | sed 's/<td align="left" bgcolor="#00FFFF">//g'| sed 's/<\/td>//g' | sed 's/<td height="17" align="left">//g' > additional-no-antigens.txt
printf "\nannotation provides additional information to that in other databases, excluding antigens:\n"
grep -Eio "Rv"[0-9]{4}[ABcD]? additional-no-antigens.txt | wc -l
printf "\n"
