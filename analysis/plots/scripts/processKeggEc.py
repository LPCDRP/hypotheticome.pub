# AUTHOR: Deepika Gunasekaran
# TITLE: Process KEGG EC
# DESCRIPTION: This script takes as input the 3rd column from kegg_annotations file and returns EC numbers in separate lines
# NOTE: All paths in this script are hard-coded
# NOTE: This script is called in ec_unique_all.mk

input_file = open('/home/dgunasek/projects/hypotheticome/analysis/plots/temp/biocyc_ec_raw', 'r').readlines()
ec_numbers_kegg = []
for line in input_file:
	line = line.strip()
	if not '.' in line:
		line = line + '.-.-.-'
		ec_numbers_kegg.append(line)
	else:
		line_elements = line.split('.')
		if len(line_elements) == 4:
			ec_numbers_kegg.append(line)
		elif len(line_elements) == 3:
			line = line + '.-'
			ec_numbers_kegg.append(line)
		elif len(line_elements) == 2:
			line = line + '.-.-'
			ec_numbers_kegg.append(line)

for i in ec_numbers_kegg:
	print(i)
 
