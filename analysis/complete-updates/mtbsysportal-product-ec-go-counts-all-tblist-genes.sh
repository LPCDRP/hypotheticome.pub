#!/bin/bash
printf "\nNumber of genes analyzed (number of protein coding genes in TubercuList): 4038\n\n"
printf "In Mtb Network Portal:\n"
products=$(echo $(cut -f3 ../../res/mtbsysportalGOECproductalltblistgenes.tsv | grep [a-z] | wc -l))
hypo=$(echo $(cut -f3 ../../res/mtbsysportalGOECproductalltblistgenes.tsv | grep -e "hypothe" -e unknown -e "conserved protein" -e uncharacterized | wc -l))
printf "\nnumber of proteins with functional assignments: "
printf $((products-hypo))
printf "\n\nhypothetical proteins: "
printf $((4038-products+hypo))
printf "\n\nproteins annotated with at least one go term: "
cut -f5,6,7 ../../res/mtbsysportalGOECproductalltblistgenes.tsv | grep "GO:" | wc -l
printf "\nproteins annotated with ec numbers: "
cut -f4,5 ../../res/mtbsysportalGOECproductalltblistgenes.tsv | grep "EC" | wc -l
printf "\n*****\n\n"

printf "In TubercuList:\n"
printf "\nproteins annotated with at least one go term: "
cut -f3 tuberculist-EC-go-all-tblist-genes.tsv | grep "GO:" | wc -l
printf "\nproteins annotated with ec numbers: "
cut -f2 tuberculist-EC-go-all-tblist-genes.tsv | grep [0-9.-] | wc -l
printf "\n"
