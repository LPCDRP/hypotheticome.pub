#!/bin/bash
grep -B 1 "#FFFF00" devils-advocate.html | sed 's/<td align="left" bgcolor="#FFFF00">//g'| sed 's/<\/td>//g' | sed 's/<td height="17" align="left">//g' > missing.txt
printf "\nannotation missing in other databases:\n"
grep -Eio "Rv"[0-9]{4}[ABcD]? missing.txt | wc -l
grep -B 1 "#FF00FF" devils-advocate.html | sed 's/<td align="left" bgcolor="#FF00FF">//g'| sed 's/<\/td>//g' | sed 's/<td height="17" align="left">//g' > stronger.txt
printf "\nannotation stronger than that in other databases:\n"
grep -Eio "Rv"[0-9]{4}[ABcD]? stronger.txt | wc -l
grep -B 1 "#00FFFF" devils-advocate.html | sed 's/<td align="left" bgcolor="#00FFFF">//g'| sed 's/<\/td>//g' | sed 's/<td height="17" align="left">//g' > additional.txt
printf "\nannotation provides additional information to that in other databases:\n"
grep -Eio "Rv"[0-9]{4}[ABcD]? additional.txt | wc -l
printf "\n"
