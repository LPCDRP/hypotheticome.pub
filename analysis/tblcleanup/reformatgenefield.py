#!/usr/bin/env python
import sys
import fileinput
import os

ambiguousgenelist = open("../../res/tblist-hypotheticome.tsv", 'U')
ambiguousgenelist = ambiguousgenelist.readlines()
start = ""
end = ""
cds = ""
locustag = ""
genepresent = False
for Rv in range(1, len(ambiguousgenelist)):
    for line in fileinput.input("../../../Mtb-H37Rv-annotation/features/" + ambiguousgenelist[Rv].split()[0] + ".tbl", inplace=1):
        if '\tCDS' in line:
            start = line.split("\t")[0]
            end = line.split("\t")[1]
            cds = line
        elif "locus_tag\t" in line:
            locustag = line
        elif "gene\t" in line:
            replace = start + "\t" + end + "\t" + "gene" + "\n" + line + locustag + cds
            print line.replace(line, replace),
            genepresent = True
        else:
            print line,
    if not genepresent:
        with open("../../../Mtb-H37Rv-annotation/features/" + ambiguousgenelist[Rv].split()[0] + ".tbl",'r') as tblfile:
            with open('placeholder.txt','w') as intermediatefile: 
                intermediatefile.write(cds + locustag)
                intermediatefile.write(tblfile.read())
        os.rename('placeholder.txt', "../../../../Mtb-H37Rv-annotation/features/" + ambiguousgenelist[Rv].split()[0] + ".tbl")
    genepresent = False

