#!/bin/bash
for tblfile in $(ls features/*.tbl); do
    for line in $tblfile; do
        if grep -qP "inference\t" $line && grep -qP "DESCRIPTION:" $line; then
	    sed -i 's/DESCRIPTION://g' $tblfile
	fi
    done
done
