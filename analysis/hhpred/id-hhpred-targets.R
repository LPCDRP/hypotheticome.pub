##### Load in packages and set themes and variables #####
library(dplyr)
library(data.table)

##### Read in dataset and filter for genes with I-TASSER annotation that lack manual annotation #####
struct_only_genes <- fread("./annotation-summary.csv", header=TRUE) %>% filter(`Tuberculist Product`==`Manual annotation Product` & `I-Tasser Product`!="")

## Isolate genes from filtered set and write to file ##
hhpred <- struct_only_genes %>% select(`Rv Number`,`I-Tasser Product`)
fwrite(hhpred, file="./HHPred-genes.csv")
