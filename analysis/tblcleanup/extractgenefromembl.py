#!/usr/bin/env python
from Bio import SeqIO
import sys
import fileinput
from collections import defaultdict
import os
import operator
import re

with open("../../../Mtb-H37Rv-annotation/manno.tbl", "U") as tblfile:
    multitbl = tblfile.read()

multitbl = re.split("(\n\d+\t\d+\t)", multitbl)
first = multitbl[0]
multitbl = [''.join([multitbl[x], multitbl[x+1]]) for x in range(1, len(multitbl)-1, 2)]
multitbl.insert(0, first)
multitbl = sorted(multitbl, key=lambda line: (int(line.split()[0]), int(line.split()[1])))

rvregex = re.compile("Rv\d{4}[ABcD]?")
CDS = ""

genedict = defaultdict(list)
genenamedict = {}

locations = []
embldict = SeqIO.read("/grp/valafar/resources/H37Rv-NC_000962.3.embl", "embl")
gene = "no gene"
#embldict.features.sort(key=operator.attrgetter("location.start"))
for feature in embldict.features:
    if feature.type == 'gene':
        rvnumber = ''.join(feature.qualifiers.get('locus_tag', []))
        if feature.location.strand == 1:
            locations.append(feature.location.start+1)
            locations.append(feature.location.end)
        else:
            locations.append(feature.location.end)
            locations.append(feature.location.start+1)
        for qualifier in feature.qualifiers:
            if qualifier != "gene":
                locations.append("\t\t\t\t" + qualifier + "\t" + ''.join(feature.qualifiers.get(qualifier, [])))
            elif qualifier == "gene":
                genename = "\t\t\t\t" + qualifier + "\t" + ''.join(feature.qualifiers.get(qualifier, []))
                genenamedict[rvnumber] = genename
        genedict[rvnumber] = locations
        locations = []

combinedtbl = open("../../../Mtb-H37Rv-annotation/mannotation-with-computation.tbl", "a")
for record in multitbl:
    try:
        Rv = record.split("locus_tag\t")[1].split("\n")[0]
        for line in record:
            if "\t\t\t\tgene\t" in line:
                gene = line
        if re.search(rvregex, Rv) is not None:
            for line in fileinput.input("../../../Mtb-H37Rv-annotation/features/" + Rv + ".tbl", inplace=1):
                if "CDS" in line:
                    CDS = line.replace("\n", "")
                elif "locus_tag\t" in line:
                    try:
                        sys.stdout.write(str(genedict[Rv][0]).strip().replace(" \t", "\t"))
                        sys.stdout.write("\t"),
                        sys.stdout.write(str(genedict[Rv][1]).strip().replace(" \t", "\t"))
                        sys.stdout.write("\tgene\n")
                        if gene != "no gene":
                           print gene,
                           for field in genedict[Rv][2:]:
                               sys.stdout.write(str(field).replace(" \t", "\t"))
                               sys.stdout.write("\n")
                        else:
                           if Rv in genenamedict.keys(): 
                               sys.stdout.write(genenamedict[Rv]) 
                               sys.stdout.write("\n")
                           for field in genedict[Rv][2:]:
                               sys.stdout.write(str(field).replace(" \t", "\t"))
                               sys.stdout.write("\n")
                        sys.stdout.flush()
                    except IndexError:
                       pass
                    print CDS.replace("\t ", "\t") + "\n" + line,
                else:
                    print line,
        with open("../../../Mtb-H37Rv-annotation/features/" + Rv + ".tbl", "U") as tblfile:
            tbl = tblfile.readlines()
        combinedtbl.writelines(tbl)
    except (OSError, IOError) as e:
        pass
    gene = "no gene"
combinedtbl.close()
