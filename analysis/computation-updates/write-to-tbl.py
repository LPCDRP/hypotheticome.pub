#!/usr/bin/env python
import re
from collections import defaultdict

mapRvEC = defaultdict(list)

rvregex = re.compile("Rv\d{4}[ABcD]?")
#productregex1 = re.compile("\t\t\t\tproduct\t[\D]+.\n")
results = []
restoffile = []
tblstr = ""


def readtbl():
    with open("../legacy_EC_files/EC-GO-reconciled.tbl", 'U') as infile:
        product = ""
        gotbl = infile.read()
        for line in gotbl.split('locus_tag'):
            restoffile = []
            try:
                rvpos = re.search(rvregex, line)
                rvnumber = line[rvpos.start():rvpos.end()]
                for x in line.split("\n"):
                    if "\tproduct\t" in x and "infer\t" not in x and "inference" not in x:
                        product = x
                    elif rvnumber not in x and "GO:" in x:
                        newgoterm = x.split("|")
                        cleanedfield = newgoterm[0] + newgoterm[1][2:].replace(":", "|") + "|" +  newgoterm[3]
                        restoffile.append(cleanedfield)
                    elif rvnumber not in x:
                        restoffile.append(x)
                results = [product, '\n'.join(restoffile)]
                mapRvEC[rvnumber] = results
                product = ""
            except AttributeError:
                pass

def writetotbl():
    for Rv in mapRvEC:
        tblstr = "../../../Mtb-H37Rv-annotation/features/" + Rv + ".tbl"
        with open(tblstr, 'U') as tblfile:
            tbl = tblfile.readlines()
            tblread = tblfile.read()
        for line in tbl:
            if "product\t" in line:
                productindex = tbl.index(line)
        if mapRvEC[Rv][0] != "":
           if tbl[productindex].lower() != mapRvEC[Rv][0]:
               if "hypothetical" not in tbl[productindex].lower():
                   tbl[productindex] = tbl[productindex].replace("\n", "") + "/" + mapRvEC[Rv][0].replace("product", "").replace("\t", "").replace("\n", "") + "\n"
               else:
                   tbl[productindex] = "\t\t\t\tproduct\t" + mapRvEC[Rv][0].replace("product", "").replace("\t", "").replace("\n", "") + "\n"
        with open(tblstr, 'w') as tblfile:
            tblfile.writelines(tbl)
        with open(tblstr, 'a') as tblfile:
            tblfile.writelines(mapRvEC[Rv][1].replace("\n\n", "\n").replace("\n\t\t\t\t\n", "\n"))
            tblfile.writelines("\n")
            
if __name__ == "__main__":
    readtbl()
    writetotbl()
