# AUTHOR: Deepika Gunasekaran
# TITLE: Flower Plot for EC presence 
# DESCRIPTION: This script plots a 5-way Venn of Rv gene ids which contain EC numbers from the following sources:
# 1. KEGG
# 2. TubercuList
# 3. Uniprot
# 4. Mannotation
# DEPENDENCIES: VennDiagram
# NOTE: All paths in this script are hard-coded
# NOTE: This script is called in ec_presence_all.mk

library(VennDiagram);
hyp_genes <- scan('temp/hypotheticome_with_ec', what = "", sep = '\n');
uniprot_hyp_genes <- scan('temp/uniprot_hyp_ec', what = "", sep = '\n');
kegg_hyp_genes <- scan('temp/kegg_hyp_ec', what = "", sep = '\n');
tblist_hyp_genes <- scan('temp/tblist_hyp_ec', what = "", sep = '\n');

consensus_venn <- draw.quad.venn(
  area1 = length(hyp_genes),
  area2 = length(uniprot_hyp_genes),
  area3 = length(kegg_hyp_genes),
  area4 = length(tblist_hyp_genes),
  n12 = length(intersect(hyp_genes,uniprot_hyp_genes)),
  n13 = length(intersect(hyp_genes,kegg_hyp_genes)),
  n14 = length(intersect(hyp_genes,tblist_hyp_genes)),
  n23 = length(intersect(uniprot_hyp_genes,kegg_hyp_genes)),
  n24 = length(intersect(uniprot_hyp_genes,tblist_hyp_genes)),
  n34 = length(intersect(kegg_hyp_genes,tblist_hyp_genes)),
  n123 = length(Reduce(intersect, list(hyp_genes,uniprot_hyp_genes,kegg_hyp_genes))),
  n124 = length(Reduce(intersect, list(hyp_genes,uniprot_hyp_genes,tblist_hyp_genes))),
  n134 = length(Reduce(intersect, list(hyp_genes,kegg_hyp_genes,tblist_hyp_genes))),
  n234 = length(Reduce(intersect, list(uniprot_hyp_genes,kegg_hyp_genes,tblist_hyp_genes))),
  n1234 = length(Reduce(intersect, list(hyp_genes,uniprot_hyp_genes,kegg_hyp_genes,tblist_hyp_genes))),
  category = c("Hypo.","UniProt","KEGG","TubercuList"),
  fill = c("dodgerblue","darkorange1","seagreen3","orchid3"),
  cat.col = c("dodgerblue","darkorange1","seagreen3","orchid3"), 
  cat.cex = 2,
#  cat.pos = 0,
#  cex = c(1.5,1.5,1.5,1.5,1,1,1,1,1,1,0.8,0.8,0.8,1.5,0.8),
#  cat.just=list(c(0.5,0) , c(1,0) , c(0,0) , c(1,1)),
  margin = 0.05,
  ind = TRUE,
)


png(filename = "ec_presence_hyp_genes");
grid.draw(consensus_venn)
dev.off()
