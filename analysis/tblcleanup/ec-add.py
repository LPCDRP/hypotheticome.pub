#!/usr/bin/env python
import re

mapRvtbl = {}
mapRvProduct = {}

rvregex = re.compile("Rv\d{4}A?c?B?D?")

with open("/home/smodlin/projects/Hypotheticome/Mtb-H37Rv-annotation/features/EC-RECON-proto-FINAL.tbl", 'U') as infile:
    ECtbl = infile.read()

def extractecfromtbl():
    for line in ECtbl.split('locus_tag'):        
        try:
            rvpos = re.search(rvregex, line)
            rvnumber = line[rvpos.start():rvpos.end()]
            mapRvtbl[rvnumber] = line[rvpos.end():len(line)] + "\n"
        except AttributeError:
            print "None Found"
        #mapRvProduct[rvnumber] = productindex + "\n")
        
def writetotbl():
    for Rv in mapRvtbl:
        tblstr = "features/" + Rv + ".tbl"
        with open(tblstr, 'U') as tblfile:
            tbl = tblfile.readlines()
            try:
                ECproductindex = next(mapRvtbl[Rv].index(line) for line in tbl if "product" in line)
                productindex = next(tbl.index(line) for line in tbl if "product" in line)
                if tbl[productindex].lower() != ECtbl[ECproductindex].lower():
                    tbl[productindex] = tbl[productindex].replace("\n", "") + '/' + ECtbl[ECproductindex]
                with open(tblstr, 'w') as tblfile:
                    tblfile.writelines(tbl)
                with open(tblstr, 'a') as tblfile:
                    tblfile.writelines(mapRvtbl[Rv][productindex:len(mapRvtbl[Rv])])
            except ValueError:
                print "None Found"


if __name__ == "__main__":
    extractecfromtbl()
    writetotbl()
