# Functions used throughout this project

define usage
# Description
printf "%0.s=" $$(seq 1 80)
echo "$1" | fold -s -w 80
printf "%0.s=" $$(seq 1 80)
# Targets
# Use one line per target, with a tab separating the target and its description
echo "$2" | column -t -s '	'
endef

define describe-target
targets:=$(targets)\n$1	$2
endef
