#!/usr/bin/env python
# EC number
from collections import defaultdict
import commands
import os
import sys
from xml.dom import minidom
from multiprocessing import Pool
import math
import re
import csv
import collections
#import operator

def extraction():
	GROUPHOME = os.environ['GROUPHOME']
	current_directory = os.getcwd()  # Check the current directory
	computation_updates = current_directory+ '/../computation-updates'
	result_directory = computation_updates + "/results/"
	print result_directory
	if not os.path.exists(result_directory):  # Check if you have result folder in this directory, if not, then create one
		os.makedirs(result_directory)
	Rv_list = open('../../res/tblist-hypotheticome.tsv', "U")
	for line in Rv_list:
		line = line.split()
		RV = line[0]
		RV = RV.strip()  # get rid of blank space in case
		template_dict = {}
		template = ""
		TM_score = 0
		SID = 0
		template_length = 0
		new_template = ""
		Title = "Template" + "\t" + "TM-Score" + "\t" + "IDEN" + "\t" + "E.C. Number" + '\t' + 'Geometric Mean' + '\n'
		input_path = GROUPHOME + "/data/depot/hypotheticome/tuberculist/" + RV + "/model1/cofactor/PDBsearchresult_ext_model1.dat"
		report = ""
		if os.path.isfile(input_path):
			report = open(input_path, "U")
		else:
			print "No file for " + RV
		for line in report:
			EC_number = ""  # EC number, can be multiple
			parameter = line.split(
				',')  # ['Template=3c1zA	 ', ' Query=model1.pdb', ' Lch_t= 349', ' Lch_q= 358', ' TM-score_q= 0.9504', ' TM-score_t= 0.9746', ' ALN= 346', ' SID= 0.379\n']
			template = parameter[0][9:]  # Gives the template
			template = template.strip()
			TM_score = float(parameter[4][13:])  # Gives TM_score
			SID = float(parameter[7][6:])  # Gives SID aka IDEN value
			template_dict[template] = [TM_score,
									   SID]  # Create dictionary with key of Template and value of TM_score and SID
			template_length = len(template)  # know the template length so you can add a dot in next line
			new_template = template[0:template_length - 1] + "." + template[template_length - 1]  # Refer last line
			XML = commands.getoutput(
				"curl -s http://www.rcsb.org/pdb/rest/describeMol?structureId=" + new_template + " >intermediate.xml")  # This will tell terminal to look for the current looping template xml file
			xmldoc = minidom.parse("intermediate.xml")  # parse the xml file
			EClist = xmldoc.getElementsByTagName('enzClass')  # List of the EC number in this template
			number_EC = len(EClist)  # how many EC number in the EC list
			EC = []  # This is going to be the EC of the template
			geom_mean = math.sqrt(TM_score * SID)
			for i in range(number_EC):
				EC.append(EClist[i].attributes['ec'].value)  # Append all the EC number to this EC list
			if EC:
				for j in range(number_EC):
					if j == 0:
						EC_number += EC[j]
					else:
						EC_number += "/" + EC[j]
			if TM_score > 0.0:
				Title += "\t".join([template, str(TM_score), str(SID), EC_number, str(geom_mean)]) + "\n"
		output = RV + "_output.csv"
		print output + " result successfully created"
		fout = open(result_directory + output, 'w')
		fout.write(Title)
		fout.close()

def GO_extract():
	# Collects the GO annotations for all the genes
	MF_thresholds_file = open('../experiment-updates/GO_MF_thresholds', "U")  # Generated in Bayesian_plots.R
	MF_thresholds = MF_thresholds_file.read()
	BP_thresholds_file = open('../experiment-updates/GO_BP_thresholds', "U")  # Generated in Bayesian_plots.R
	BP_thresholds = BP_thresholds_file.read()
	CC_thresholds_file = open('../experiment-updates/GO_CC_thresholds', "U")  # Generated in Bayesian_plots.R
	CC_thresholds = CC_thresholds_file.read()
	# MF_prb = MF_thresholds.split()[2]
	# MF_put = MF_thresholds.split()[1]
	MF_pot = MF_thresholds.split('\t')[0]
	# BP_prb = BP_thresholds.split()[2]
	BP_put = BP_thresholds.split('\t')[1]
	BP_pot = BP_thresholds.split('\t')[0]
	# CC_prb = CC_thresholds[2]
	# CC_put = CC_thresholds[1]
	CC_pot = CC_thresholds.split('\t')[0]
	# For each category, generates a dictionary of lists  where the key is the locus tag and the values are a list of GO
	# annotations above the given confidence thresholds
	MF_significant = defaultdict(list)
	BP_put_significant = []
	BP_pot_significant = defaultdict(list)
	CC_significant = defaultdict(list)
	with open("../experiment-updates/Molecular_function_GO.tsv", "U") as all_mf_annotations:
		for line in all_mf_annotations:
			if line.split('\t')[2] > MF_pot:
				annotation_string = "go_function\t" + ' '.join(line.split('\t')[3:-1]) + "|" + line.split('\t')[1][3:] + "\n"
				rvnumber = line.split('\t')[0]
				MF_significant[rvnumber].append(annotation_string)
	with open("../experiment-updates/Biological_process_GO.tsv", "U") as all_bp_annotations:
		for line in all_bp_annotations:
			if line.split('\t')[2] > BP_put:
				gene = line.split('\t')[0]
				GO_ID = line.split('\t')[1]
				GO_desc = line.split('\t')[3]
				#annotation_string = "\t\t\t\tlocus_tag\t" + line.split()[0] + "\n\t\t\t\tproduct\tputative " + ' '.join(line.split()[3:-1]) + "\n\t\t\t\tgo_process\t" + ' '.join(line.split()[3:-1]) + "|" + line.split()[1][3:] + "\n\n"
				annotation_string = "\t\t\t\t" + 'locus_tag' + "\t" + gene + "\n" + "\t\t\t\t" + 'go_process' + "\t" + GO_desc + '|' + GO_ID + '||' + 'ISS' + "\n" + "\t\t\t\t" + 'note' + "\t" + 'identified by profile of predicted tertiary structure' + "\n" + "\t\t\t\t" + 'inference' + "\t" + 'ab initio prediction:I-TASSER:5.1'  + "\n" + "\t\t\t\t" + 'inference' + "\t" + 'profile:COACH' + "\n\n"
				BP_put_significant.append(annotation_string)
		#	elif line.split()[2] > BP_pot:
		#		annotation_string = "go_process\t" + ' '.join(line.split()[3:-1]) + "|" + line.split()[1][3:] + "\n"
				#rvnumber = line.split()[0]
				#BP_pot_significant[rvnumber].append(annotation_string)
	#with open("../experiment-updates/Cellular_component_GO.tsv", "U") as all_cc_annotations:
	#for line in all_cc_annotations:
	#	if line.split()[2] > CC_pot:
	#		annotation_string = "go_component\t" + ' '.join(line.split()[3 - 1]) + "|" + line.split()[1][3:] + "\n"
	#		rvnumber = line.split()[0]
	#		CC_significant[rvnumber].append(annotation_string)
	putproducts = open("Putative_BP_products.tbl", "w")
	putproducts.writelines(BP_put_significant)
			# Writes the GO annotations (COACH) to a file
  
def annotation_extraction():
	GROUPHOME = os.environ['GROUPHOME']
	current_directory = os.getcwd()  # Check the current directory
	computation_updates = current_directory+ '/../computation-updates'
	result_directory = computation_updates + "/results/"
	template_go_directory = computation_updates + '/template_GO_term/'
	print template_go_directory
	header = ['Template' , 'TM-Score' , 'IDEN' , 'E.C. Number' , 'Geometric Mean' , 'Property' ,'GO Term' ,'Function Name']
	if not os.path.exists(template_go_directory):  # Check if you have result folder in this directory, if not, then create one
		os.makedirs(template_go_directory)
	with open('../experiment-updates/all-product-output.tsv', 'U') as Rv_list:
		for line in Rv_list:
			line=line.strip()
			file_name = line.split('/')[-1]
			name = line.split('/')[-1].split('_')[0]
			print name
			with open(template_go_directory+ name +'_PDB_GO.csv', 'w') as output:
				output.write('\t'.join(header)+ '\n')
				with open(result_directory + file_name, 'U') as csvfile:
					reader = csv.reader(csvfile, delimiter='\t',quotechar='|',quoting=csv.QUOTE_MINIMAL) #able to read csv
					next(reader)
					for line in reader:
						GO_function = ""
						GO_dict = collections.OrderedDict()
						template = line[0][0:len(line[0])-1] + '.' + line[0][-1]
						if template[-1].isdigit() == True:
							template = line[0][0:len(line[0])-2] + '.' + line[0][-2]
						XML = commands.getoutput("curl -s http://www.rcsb.org/pdb/rest/goTerms?structureId=" + template + " >intermediate.xml")  # This will tell terminal to look for the current looping template xml file
						xmldoc = minidom.parse("intermediate.xml")  # parse the xml file
						term = xmldoc.getElementsByTagName('term')  # look for term tag
						try:
							for i in term:
								GO = str(i.toxml()).split('id="')[1].split('"')[0]
								name = str(i.toxml()).split('name="')[1].split('"')[0]
								ontology = str(i.toxml()).split('ontology="')[1].split('"')[0]
								if ontology in GO_dict.keys():
									GO_dict[ontology].append([GO, name])
								else:
									GO_dict[ontology]=[[GO, name]]

							for i in GO_dict['F']:
								GO_function += '\t'.join(line) + '\t'+ 'Molecular Function' + '\t'+  i[0] + '\t'+ i[1] + '\n'
							for i in GO_dict['B']:
								GO_function += '\t'.join(line) + '\t'+'Biological Process' + '\t'+  i[0] + '\t'+ i[1] + '\n'
							for i in GO_dict['C']:
								GO_function += '\t'.join(line) + '\t'+ 'Cellular Component' + '\t'+  i[0] + '\t'+ i[1] + '\n'
						except:
							pass
						output.write(GO_function)

def ITASSER_extract():
# EC numbers via PDB structural similarity
	EC_gene_path = '../../../Mtb-H37Rv-annotation/features/' + 'EC.tbl'
	open(EC_gene_path, "w+")
	GO_gene_path = '../../../Mtb-H37Rv-annotation/features/' + 'GO_PDB.tbl'
	open(GO_gene_path, "w+")
	hypotheticome_set = open('../../res/tblist-hypotheticome.tsv', "U")
	EC_additions = open('../computation-updates/iTASSER_EC_annotation', 'w')
	GO_additions = open('../computation-updates/iTASSER_GO_annotation', 'w')
	ec_additions = {}
	go_additions = {}
	for gene in hypotheticome_set:
		line = gene.split()
		gene = line[0]
		gene = gene.strip()
		entry_list = open('../computation-updates/results/' + gene + '_output.csv', "U")  # What is the U for??
                entry_list_2 = open('../computation-updates/template_GO_term/' + gene + '_PDB_GO.csv', "U")  # What is the U for??
		EC_prb_pre = open('../experiment-updates/EC_prb', "U")  # Generated in Bayesian_plots.R
		EC_prb = EC_prb_pre.readlines()[0]
		EC_put_pre = open('../experiment-updates/EC_put', "U")  # Generated in Bayesian_plots.R
		EC_put = EC_put_pre.readlines()[0]
		EC_pot_pre = open('../experiment-updates/EC_pot', "U")  # Generated in Bayesian_plots.R
		EC_pot = EC_pot_pre.readlines()[0]
		pot_list = []
		put_list = []
		prb_list = []
		i = 0
		go_desc = {}
		go_dict = {}
		for entry in entry_list:
			if i == 0:
				i = i + 1
				continue
			TMID = float(entry.split('\t')[4])
			ECnum = entry.split('\t')[3]
			PDB = entry.split('\t')[0]
			split_list = ECnum.split('/')
			for EC in split_list:
				#print EC
				if re.match('\d', EC) == None:
					continue
				elif TMID >= float(EC_prb.split('\t')[3]):
					prb_list = prb_list + [PDB + '|' + EC]
					continue
				elif float(EC_put.split('\t')[3]) <= TMID and TMID <= float(EC_prb.split('\t')[3]):
					put_list = put_list + [PDB + '|' + EC]
				elif float(EC_pot.split('\t')[3]) <= TMID and TMID <= float(EC_put.split('\t')[3]):
					pot_list = pot_list + [PDB + '|' + EC]
				if TMID >= float(EC_prb.split('\t')[2]):
					prb_list = prb_list + [PDB + '|' + ".".join(EC.split('.')[0:3]) + '.' + '-']
					continue
				elif float(EC_put.split('\t')[2]) <= TMID and TMID <= float(EC_prb.split('\t')[2]):
					put_list = put_list + [PDB + '|' + ".".join(EC.split('.')[0:3]) + '.' + '-']
				elif float(EC_pot.split('\t')[2]) <= TMID and TMID <= float(EC_put.split('\t')[2]):
					pot_list = pot_list + [PDB + '|' + ".".join(EC.split('.')[0:3]) + '.' + '-']
				if TMID >= float(EC_prb.split('\t')[1]):
					prb_list = prb_list + [PDB + '|' + ".".join(EC.split('.')[0:2]) + '.' + '-' + '.' + '-']
					continue
				elif float(EC_put.split('\t')[1]) <= TMID and TMID <= float(EC_prb.split('\t')[1]):
					put_list = put_list + [PDB + '|' + ".".join(EC.split('.')[0:2]) + '.' + '-' + '.' + '-']
				#elif float(EC_pot.split('\t')[1]) <= TMID and TMID <= float(EC_put.split('\t')[1]):
					#pot_list = pot_list + [".".join(EC.split('.')[0:2]) + '.' + '-' + '.' + '-']
				#if TMID >= float(EC_prb.split('\t')[0]):
					#prb_list = prb_list + [".".join(EC.split('.')[0]) + '.' + '-' + '.' + '-' + '.' + '-']
				else:
					pass
		for entry in entry_list_2:
                        entry= entry.strip('\n')
                        entry_components = entry.split('\t')
                        GO_type = entry_components[5]
                        GO_ID = entry_components[6]
                        GO_desc = entry_components[7]
                        PDB = entry_components[0]
                        #print(GO_ID + ': ' + GO_desc + ': ' + str(TMID))       #Testing iterative GO incorporation
                        if GO_desc == 'molecular_function' or GO_desc == 'cellular_component' or GO_desc == 'biological_process':
                                continue
                        else:
                                if GO_ID not in go_desc.keys():
                                        go_desc[GO_ID] = {PDB: [GO_type, GO_desc]}
                                        go_dict[GO_ID] = [GO_type, GO_desc]
                                elif GO_ID in go_desc.keys() and PDB not in go_desc[GO_ID].keys():
                                        go_desc[GO_ID][PDB] = [GO_type, GO_desc]
                                else:
                                        continue
		pan_list = pot_list + put_list + prb_list
		#print gene
		#print pan_list
		potential_ec_list = []
		putative_ec_list = []
		probable_ec_list = []
		ec_pdb_dict = {}
		for i in pot_list:
			ec = i.split('|')[1]
			pdb = i.split('|')[0]
			if ec not in ec_pdb_dict.keys():
				ec_pdb_dict[ec] = [pdb]
			else:
				ec_pdb_dict[ec].append(pdb)
			if ec not in potential_ec_list:
				potential_ec_list.append(ec)
		for i in put_list:
			ec = i.split('|')[1]
                        if ec not in ec_pdb_dict.keys():
                                ec_pdb_dict[ec] = [pdb]
                        else:
                                ec_pdb_dict[ec].append(pdb)
			if ec not in putative_ec_list:
				putative_ec_list.append(ec)
		for i in prb_list:
			ec = i.split('|')[1]
                        if ec not in ec_pdb_dict.keys():
                                ec_pdb_dict[ec] = [pdb]
                        else:
                                ec_pdb_dict[ec].append(pdb)
			if ec not in probable_ec_list:
				probable_ec_list.append(ec)	
		uniq_ec = list(set(pan_list))
		uniq_ec_dict = {} 
		for i in uniq_ec:
			uniq_ec_comp = i.split('|')
			### This step is to check formatting of EC numbers. All EC numbers should have a length of 7
			check_validity_ec = len(uniq_ec_comp[1])
			valid_ec = uniq_ec_comp[1]
			if valid_ec[-1] == '.':
				#print 'Invalid EC: ' + valid_ec
				#print valid_ec
				split_ec = valid_ec.split('.')
				if split_ec[-1] == '' and len(split_ec) < 4:
					valid_ec = valid_ec + '-'
				elif split_ec[-1] == '' and len(split_ec) > 4:
					valid_ec = '.'.join(split_ec[:4])
			check_validity_ec = valid_ec.count('.')
			if valid_ec[-1] == '.':
				validate_ec = {2: '-.-', 1: '-.-.-'}
			else:
				validate_ec = {2: '.-', 1: '.-.-', 0: '.-.-.-'}
			if check_validity_ec < 3:
				#print 'Invalid EC: ' + valid_ec
				valid_ec = valid_ec + validate_ec[check_validity_ec]
				#print 'Valid EC: ' + valid_ec
			else:
				valid_ec = uniq_ec_comp[1]
			if valid_ec not in uniq_ec_dict.keys():
				uniq_ec_dict[valid_ec] = [uniq_ec_comp[0]]
			else:
				uniq_ec_dict[valid_ec].append(uniq_ec_comp[0])
		uniq_ec = uniq_ec_dict.keys()
		#print(gene + ': ' + '|'.join(uniq_ec))
		ec = {'Potential': [], 'Putative': [], 'Probable': []}
		specificity = 0
		specific_ec_list = []
		for i in uniq_ec:
			ec_count = (4 - i.count('-'))
			#print ec_count
			specificity = max((ec_count), specificity)
			#print specificity
		for i in uniq_ec:
			if (4 - i.count('-')) == specificity and i not in specific_ec_list:  #
				specific_ec_list.append(i)  
		#print(gene + ': ' + '|'.join(specific_ec_list))
		
		#############################################################################################################
		### This step generates a list of partial EC numbers from the specific EC nimber list. This is to prevent ###
		### redundancy of EC number addition to the tbl files #######################################################
		#############################################################################################################
		incomplete_ec = []
		validate_ec = {3: '-', 2: '-.-', 1: '-.-.-'}
		for j in uniq_ec:
			specificity = (4 - j.count('-'))
			if specificity == 1:
				continue
			else:
				j_split = j.split('.')
				#print j_split
				#print specificity
				for spec in range(1,specificity):
					#print(spec)
					partial_ec = ''
					for k in range(0,spec):
						partial_ec = partial_ec + j_split[k] + '.'
					#print(partial_ec)	
					partial_ec = partial_ec + validate_ec[spec]
					if partial_ec not in incomplete_ec:
						incomplete_ec.append(partial_ec)
		#print(gene + ': ' + '|'.join(incomplete_ec))
		checked_ec = []
		for i in uniq_ec:
			# If EC number is in the list of the most specific EC number for this gene
			if i in specific_ec_list and i not in checked_ec:
				checked_ec.append(i)				
				if i in probable_ec_list:
					ec['Probable'].append(i)
				elif i in putative_ec_list:
					ec['Putative'].append(i)
				elif i in potential_ec_list:
					ec['Potential'].append(i)
			# If the EC number is the first instance of a partial EC number, for which a more specific EC is in the list of Unique ECs
			elif i in incomplete_ec and i not in checked_ec:
				checked_ec.append(i)
			# Partial EC number has already been ancountered
			elif i in incomplete_ec and i in checked_ec:
				continue
			# A partial EC number (not represented in the mose specific EC number list)
			elif i not in incomplete_ec and i not in checked_ec:
				checked_ec.append(i)
				if i in probable_ec_list:
					ec['Probable'].append(i)
				elif i in putative_ec_list:
					ec['Putative'].append(i)
				elif i in potential_ec_list:
					ec['Potential'].append(i)
		#print ec
		EC_tbl_file = open(EC_gene_path, 'a')
		gene_head = "\t\t\t\t" + 'locus_tag' + "\t" + gene + "\n"
		EC_tbl_file.write(gene_head)
		valid_pdb = []
		for EC in ec['Probable']:
			if len(ec['Probable']) == 0:
				continue
			else:
			#############################################################################################################
        	        ### This step looks into the uniq_ec_dict to retrieve tha pdb ids for all EC/partial ECs in the dictionary ##
			#############################################################################################################
				pdb_list = []
				if '-' in EC:
					ec_number = EC.split('.-')[0]
				else:
					ec_number = EC
				ec_length = len(ec_number)
				for k in uniq_ec_dict.keys():
					if k[:ec_length] == ec_number:
						for p in uniq_ec_dict[k]:
							if p not in valid_pdb:
								valid_pdb.append(p)
							if p not in pdb_list:
								pdb_list.append(p)
							else:
								continue
				pdb = ','.join(pdb_list)
				#print(pdb)
				ec_string = "\t\t\t\t" + 'EC_number' + "\t" + EC + ' (Probable)' + "\n" + "\t\t\t\t" + 'note' + "\t" + 'identified by similarity of predicted tertiary structure to PDB:' + pdb + "\n" + "\t\t\t\t" + 'inference' + "\t" + 'ab initio prediction:I-TASSER:5.1'  + "\n\n"
				EC_tbl_file.write(ec_string)
				#print ec_string
				if gene not in ec_additions.keys():
					ec_additions[gene] = 'Probable'
		for EC in ec['Putative']:
			if len(ec['Putative']) == 0:
				continue
			else:
                        #############################################################################################################
                        ### This step looks into the uniq_ec_dict to retrieve tha pdb ids for all EC/partial ECs in the dictionary ##
                        #############################################################################################################
                                pdb_list = []
                                if '-' in EC:
                                        ec_number = EC.split('.-')[0]
                                else:
                                        ec_number = EC
                                ec_length = len(ec_number)
                                for k in uniq_ec_dict.keys():
                                        if k[:ec_length] == ec_number:
                                                for p in uniq_ec_dict[k]:
							if p not in valid_pdb:
								valid_pdb.append(p)
                                                        if p not in pdb_list:
                                                                pdb_list.append(p)
                                                        else:
                                                                continue
                                pdb = ','.join(pdb_list)
                                #print(pdb)
				ec_string = "\t\t\t\t" + 'EC_number' + "\t" + EC + ' (Putative)' + "\n" + "\t\t\t\t" + 'note' + "\t" + 'identified by similarity of predicted tertiary structure to PDB:' + pdb + "\n" + "\t\t\t\t" + 'inference' + "\t" + 'ab initio prediction:I-TASSER:5.1'  + "\n\n"
				EC_tbl_file.write(ec_string)
				#print ec_string
				if gene not in ec_additions.keys():
					ec_additions[gene] = 'Putative'
		#print gene
		#print ec
		#############################################################################################################
                ################# This step looks through GO annotations for a PDB id and adds it to EC.tbl #################
                #############################################################################################################
                for go_term in go_dict.keys():
			go_pdbs = []
                        for p in valid_pdb:
				if p in go_desc[go_term].keys():
					go_pdbs.append(p)
			if len(go_pdbs) == 0:
				continue
			pdb = ','.join(go_pdbs)
			if go_dict[go_term][0] == 'Molecular Function': # For MF
                                go_string = "\t\t\t\t" + 'go_function' + "\t" + go_dict[go_term][1] + '|' + go_term + '||' + 'ISS' + "\n" + "\t\t\t\t" + 'note' + "\t" + 'identified by similarity of predicted tertiary structure to PDB ' + pdb + "\n" + "\t\t\t\t" + 'inference' + "\t" + 'ab initio prediction:I-TASSER:5.1'  + "\n" + "\t\t\t\t" + 'inference' + "\t" + 'alignment:TM-align:PDB:' + pdb  + "\n"
                                #print go_string
                                EC_tbl_file.write(go_string)
                        elif go_dict[go_term][0] == 'Biological Process': # For BP
                                go_string = "\t\t\t\t" + 'go_process' + "\t" + go_dict[go_term][1] + '|' + go_term + '||' + 'ISS' + "\n" + "\t\t\t\t" + 'note' + "\t" + 'identified by similarity of predicted tertiary structure to PDB ' + pdb + "\n" + "\t\t\t\t" + 'inference' + "\t" + 'ab initio prediction:I-TASSER:5.1'   + "\n" + "\t\t\t\t" + 'inference' + "\t" + 'alignment:TM-align:PDB:' + pdb  + "\n"
                                EC_tbl_file.write(go_string)
                                #print(go_string)
                        elif go_dict[go_term][0] == 'Cellular Component': # For CC
                                go_string = "\t\t\t\t" + 'go_component' + "\t" + go_dict[go_term][1] + '|' + go_term + '||' + 'ISS' + "\n" + "\t\t\t\t" + 'note' + "\t" + 'identified by similarity of predicted tertiary structure to PDB ' + pdb + "\n" + "\t\t\t\t" + 'inference' + "\t" + 'ab initio prediction:I-TASSER:5.1'   + "\n" + "\t\t\t\t" + 'inference' + "\t" + 'alignment:TM-align:PDB:' + pdb  + "\n"
                                EC_tbl_file.write(go_string)
                                #print(go_string)

		
# GO via structural similarity to characterized structure on PDB
		EC_prb_pre = open('../experiment-updates/EC_prb', "U")  # Thresholds for Generated in Bayesian_plots.R
		EC_prb = EC_prb_pre.readlines()[0] # Reading in first (and only) line of EC_put thresholds
		GO_tbl_file = open(GO_gene_path, 'a')		
		gene_head = "\t\t\t\t" + 'locus_tag' + "\t" + gene + "\n" 
		GO_tbl_file.write(gene_head)				
		i = 0
		go_pdb_dict = {}
		go_dict = {}
                entry_list_2 = open('../computation-updates/template_GO_term/' + gene + '_PDB_GO.csv', "U")  # What is the U for??	
		go_term_threshold = float(EC_prb.split('\t')[2])
		prb_3 = float(EC_prb.split('\t')[3])
		put_3 = float(EC_put.split('\t')[3])
		prb_2 = float(EC_prb.split('\t')[2])
		put_2 = float(EC_put.split('\t')[2])
		all_gene_with_EC_GO=[]
		with open('../computation-updates/all_gene_with_EC_GO.txt', 'U') as filehandler:
                        for line in filehandler:
                                line=line.strip()
                                all_gene_with_EC_GO.append(line)
		#print(go_term_threshold)
		go_tmid_t1 = {}	# Dict for GO Terms that pass threshold of probable EC with specificity 3
		go_tmid_t2 = {}	# Dict for GO Terms that pass threshold of putative EC with specificity 3 but not probable EC with specificity 3
		go_tmid_t3 = {} # Dict for GO Terms that pass threshold of probable EC with specificity 2 but not putative EC with specificity 3
		go_tmid_t4 = {}	# Dict for GO Terms that pass threshold of putative EC with specificity 2 but not probable EC with specificity 2
		go_desc_t1 = {}
		go_desc_t2 = {}
		go_desc_t3 = {}
		go_desc_t4 = {}
		#print(gene)
		if gene in all_gene_with_EC_GO:
			#print('Gene annotated with manual curation or EC')	#Testing iterative GO incorporation
			continue
		for entry in entry_list_2:		#Skip the header
			if i == 0:
				i = i + 1
				continue
			entry= entry.strip('\n')
			entry_components = entry.split('\t')
			#print(entry_components)
			TMID = float(entry_components[4]) # separate each entry by tabs
			GO_type = entry_components[5]
			GO_ID = entry_components[6]
			GO_desc = entry_components[7]
			PDB = entry_components[0]
			#print(GO_ID + ': ' + GO_desc + ': ' + str(TMID))	#Testing iterative GO incorporation
			if GO_desc == 'molecular_function' or GO_desc == 'cellular_component' or GO_desc == 'biological_process':
                        	continue
			if TMID >= prb_3:
				if GO_ID not in go_tmid_t1.keys():
					go_tmid_t1[GO_ID] = TMID
					go_desc_t1[GO_ID] = {PDB: [GO_type, GO_desc]}
				elif GO_ID in go_tmid_t1.keys() and PDB not in go_desc_t1[GO_ID].keys():
					go_desc_t1[GO_ID][PDB] = [GO_type, GO_desc]
				elif GO_ID in go_tmid_t1.keys() and PDB in go_desc_t1[GO_ID].keys() and TMID > go_tmid_t1[GO_ID]:
					go_tmid_t1[GO_ID] = TMID
					go_desc_t1[GO_ID] = {PDB: [GO_type, GO_desc]}
				else:
					continue
			elif TMID < prb_3 and TMID >= put_3:
				if GO_ID not in go_tmid_t2.keys():
					go_tmid_t2[GO_ID] = TMID
					go_desc_t2[GO_ID] = {PDB: [GO_type, GO_desc]}
				elif GO_ID in go_tmid_t2.keys() and PDB not in go_desc_t2[GO_ID].keys():
					go_desc_t2[GO_ID][PDB] = [GO_type, GO_desc]
				elif GO_ID in go_tmid_t2.keys() and PDB in go_desc_t2[GO_ID].keys() and TMID > go_tmid_t2[GO_ID]:
					go_tmid_t2[GO_ID] = TMID
					go_desc_t2[GO_ID] = {PDB: [GO_type, GO_desc]}
				else:
					continue
			elif TMID < put_3 and TMID >= prb_2:
				if GO_ID not in go_tmid_t3.keys():
					go_tmid_t3[GO_ID] = TMID
					go_desc_t3[GO_ID] = {PDB: [GO_type, GO_desc]}
				elif GO_ID in go_tmid_t3.keys() and PDB not in go_desc_t3[GO_ID].keys():
					go_desc_t3[GO_ID][PDB] = [GO_type, GO_desc]
				elif GO_ID in go_tmid_t3.keys() and PDB in go_desc_t3[GO_ID].keys() and TMID > go_tmid_t3[GO_ID]:
					go_tmid_t3[GO_ID] = TMID
					go_desc_t3[GO_ID] = {PDB: [GO_type, GO_desc]}
				else:
					continue
			elif TMID < prb_2 and TMID >= put_2:
				if GO_ID not in go_tmid_t4.keys():
					go_tmid_t4[GO_ID] = TMID
					go_desc_t4[GO_ID] = {PDB: [GO_type, GO_desc]}
				elif GO_ID in go_tmid_t4.keys() and PDB not in go_desc_t4[GO_ID].keys():
					go_desc_t4[GO_ID][PDB] = [GO_type, GO_desc]
				elif GO_ID in go_tmid_t4.keys() and PDB in go_desc_t4[GO_ID].keys() and TMID > go_tmid_t4[GO_ID]:
					go_tmid_t4[GO_ID] = TMID
					go_desc_dict[GO_ID] = [PDB, GO_type, GO_desc]
				else:
					continue
			else:
				continue
		#Testing iterative GO incorporation
		#print(prb_3)
		#print(go_tmid_t1)
		#print(put_3)
		#print(go_tmid_t2)
		#print(prb_2)
		#print(go_tmid_t3)
		#print(put_2)
		#print(go_tmid_t4)
		if go_tmid_t1 == {}:
			if go_tmid_t2 == {}:
				if go_tmid_t3 == {}:
					if go_tmid_t4 == {}:
						#Testing iterative GO incorporation
						#print('No GO annotation for gene')
						continue
					else:
						#Testing iterative GO incorporation
						#print('GO annotation added from Threshold 4')
						if gene not in go_additions.keys():
							go_additions[gene] = 'Putative_2'
						for go_term in go_tmid_t4.keys():
						#For each GO ID, the following information is in the following dictionary values:
							TMID = go_tmid_t4[go_term]
							GO_ID = go_term
							PDB_list = go_desc_t4[go_term].keys()
							if GO_ID not in go_pdb_dict.keys():	# Check if GO ID is already added to the dictionary
								go_pdb_dict[GO_ID] = {}
								for PDB in PDB_list:
									GO_desc = go_desc_t4[go_term][PDB][1]
									GO_type = go_desc_t4[go_term][PDB][0]
									go_pdb_dict[GO_ID][PDB] = TMID
									go_dict[GO_ID] = [GO_type, GO_desc]
							else:
								continue
				else:
					#Testing iterative GO incorporation
					#print('GO annotation added from Threshold 3')
					if gene not in go_additions.keys():
                                        	go_additions[gene] = 'Probable_2'
					for go_term in go_tmid_t3.keys():
					#For each GO ID, the following information is in the following dictionary values:
						TMID = go_tmid_t3[go_term]
						GO_ID = go_term
						PDB_list = go_desc_t3[go_term].keys()
						if GO_ID not in go_pdb_dict.keys():	# Check if GO ID is already added to the dictionary
							go_pdb_dict[GO_ID] = {}
							for PDB in PDB_list:
								GO_desc = go_desc_t3[go_term][PDB][1]
								GO_type = go_desc_t3[go_term][PDB][0]
								go_pdb_dict[GO_ID][PDB] = TMID
								go_dict[GO_ID] = [GO_type, GO_desc]
						else:
							continue
			else:
				#Testing iterative GO incorporation
				#print('GO annotation added from Threshold 2')
				if gene not in go_additions.keys():
                                	go_additions[gene] = 'Putative_3'
				for go_term in go_tmid_t2.keys():
					#For each GO ID, the following information is in the following dictionary values:
					TMID = go_tmid_t2[go_term]
					GO_ID = go_term
					PDB_list = go_desc_t2[go_term].keys()
					if GO_ID not in go_pdb_dict.keys():	# Check if GO ID is already added to the dictionary
						go_pdb_dict[GO_ID] = {}
						for PDB in PDB_list:
							#print(go_desc_t2[go_term])
							GO_desc = go_desc_t2[go_term][PDB][1]
							GO_type = go_desc_t2[go_term][PDB][0]
							go_pdb_dict[GO_ID][PDB] = TMID
							go_dict[GO_ID] = [GO_type, GO_desc]
					else:
						continue
		else:
			#Testing iterative GO incorporation
			#print('GO annotation added form Threshold 1')
			if gene not in go_additions.keys():
                        	go_additions[gene] = 'Probable_3'
			for go_term in go_tmid_t1.keys():
				#For each GO ID, the following information is in the following dictionary values:
				TMID = go_tmid_t1[go_term]
				GO_ID = go_term
				PDB_list = go_desc_t1[go_term].keys()
				if GO_ID not in go_pdb_dict.keys():	# Check if GO ID is already added to the dictionary
					go_pdb_dict[GO_ID] = {}
					for PDB in PDB_list:
						GO_desc = go_desc_t1[go_term][PDB][1]
						GO_type = go_desc_t1[go_term][PDB][0]
						go_pdb_dict[GO_ID][PDB] = TMID
						go_dict[GO_ID] = [GO_type, GO_desc]
				else:
					continue

		GO_tbl_file = open(GO_gene_path, 'a')
		for go_term in go_pdb_dict.keys():
			pdb = ','.join(go_pdb_dict[go_term].keys())					
			if go_dict[go_term][0] == 'Molecular Function': # For MF
				go_string = "\t\t\t\t" + 'go_function' + "\t" + go_dict[go_term][1] + '|' + go_term + '||' + 'ISS' + "\n" + "\t\t\t\t" + 'note' + "\t" + 'identified by similarity of predicted tertiary structure to PDB ' + pdb + "\n" + "\t\t\t\t" + 'inference' + "\t" + 'ab initio prediction:I-TASSER:5.1'  + "\n" + "\t\t\t\t" + 'inference' + "\t" + 'alignment:TM-align:PDB:' + pdb  + "\n"
				#print go_string
				GO_tbl_file.write(go_string)				
			elif go_dict[go_term][0] == 'Biological Process': # For BP
				go_string = "\t\t\t\t" + 'go_process' + "\t" + go_dict[go_term][1] + '|' + go_term + '||' + 'ISS' + "\n" + "\t\t\t\t" + 'note' + "\t" + 'identified by similarity of predicted tertiary structure to PDB ' + pdb + "\n" + "\t\t\t\t" + 'inference' + "\t" + 'ab initio prediction:I-TASSER:5.1'   + "\n" + "\t\t\t\t" + 'inference' + "\t" + 'alignment:TM-align:PDB:' + pdb  + "\n"
				GO_tbl_file.write(go_string)				
				#print(go_string)
			elif go_dict[go_term][0] == 'Cellular Component': # For CC
				go_string = "\t\t\t\t" + 'go_component' + "\t" + go_dict[go_term][1] + '|' + go_term + '||' + 'ISS' + "\n" + "\t\t\t\t" + 'note' + "\t" + 'identified by similarity of predicted tertiary structure to PDB ' + pdb + "\n" + "\t\t\t\t" + 'inference' + "\t" + 'ab initio prediction:I-TASSER:5.1'   + "\n" + "\t\t\t\t" + 'inference' + "\t" + 'alignment:TM-align:PDB:' + pdb  + "\n"
				GO_tbl_file.write(go_string)
				#print(go_string)
	##############################################################
	##### Writing list of genes annotated by iTASSER to file #####
	##############################################################
	for gene in ec_additions.keys():
		write_string = gene + '\t' + ec_additions[gene] + '\n'
		EC_additions.write(write_string)
	EC_additions.close()
	for gene in go_additions.keys():
		write_string = gene + '\t' + go_additions[gene] + '\n'
		GO_additions.write(write_string)
	GO_additions.close()
				

#if __name__ == "__main__":
	#extraction()
	#annotation_extraction()
	#GO_extract()
	#ITASSER_extract()
