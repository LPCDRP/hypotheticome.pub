import commands
import os
import sys
import csv
def EC(RV_list):
    GROUPHOME = os.environ['GROUPHOME']
    current_directory= os.getcwd() #Check the current directory
    result_directory = current_directory+"/results/" 
    Rv_list = open(RV_list, "U")
    os.chdir("../../../Mtb-H37Rv-annotation/features") #Go to gene table folder, HOPEFULLY EVERYONE HAS SAME LOCATION
    EC_dict = {} #Set up dictionary for manual curation of the genes
    for RV in Rv_list:
        #print RV
        RV= RV.strip() #get rid of blank space in case
        EC_number = "" 
        EC_list =[]
        EC_number = commands.getoutput("grep 'EC number\t' " + RV +".tbl | cut -c15-") #grep from each gene table with  "EC number" and cut the number only
        #The following statement will sort out those doesn't have manual curation EC, and then add the EC number to the corresponding RV in dictionary
        if "." in EC_number: 
            EC_list = EC_number.splitlines()
            EC_dict[RV]= EC_list
        else:
            pass
            #print "There is no manual curated EV number for " +RV
    
    os.chdir(current_directory)
    with open("../../res/EC-Rv.tsv", 'rb') as csvfile:
        reader = csv.reader(csvfile, delimiter='\t',quotechar='|',quoting=csv.QUOTE_MINIMAL) #able to read csv
        for row in reader:
            value = row[0].split(" // ")
            EC_dict[row[1]] = value
    return EC_dict


def TM_score_EC_extraction(EC, RV_list):
    EC_dict = EC(RV_list)
    GROUPHOME = os.environ['GROUPHOME']
    current_directory= os.getcwd() #Check the current directory
    result_directory = current_directory+"/results/" 
    Rv_list = open(RV_list, "U")
    os.chdir(result_directory)  #Change directory to result directory
    #Rv_list.seek(0) #reshuffle the loop
    Title=[]
    Title.append(["RV number", "Template", "TM-Score", "IDEN" , "C-score", "E.C. Number", "Position 1", "Position 2", "Position 3", "Position 4" , "Position 1 fix", "Position 2 fix", "Position 3 fix", "Position 4 fix"]) #header
                 
    for RV in Rv_list:
        RV= RV.strip() #get rid of blank space in case
        name=RV+'_output.csv' #name of the each RV output file
        c_score_path = GROUPHOME+"/data/depot/hypotheticome/tuberculist/"+RV+"/cscore"
        try: #try to see if the following keyword exist in the EC dictionary
            report = open(c_score_path, "U")
            for line in report:
                if line.startswith("model1"):
                    line1 = line.split()
                    c_score = line1[1]
            length_EC_dict =  len(EC_dict[RV]) #Check length of the value in the key that it loop through
            EC=[]
            with open(name,'rb') as csvfile:
                reader = csv.reader(csvfile, delimiter='\t',quotechar='|',quoting=csv.QUOTE_MINIMAL) #able to read csv
                for row in reader: 
                    if "." in row[3] and "E.C. Number" not in row[3]: #so if . exist, that means that template has EC number
                        EC=row[3].split(".") 
                        EC=row[3].split("/") #in case there is multiple EC number
                        if len(EC)>1:
                            #The following statement will duplicate the I-TASSER row with multiple EC number
                            for i in range(len(EC)):

                                Title.append([RV,row[0],row[1],row[2],c_score, EC[i]]) #append according to RV number, Template, TM-score, IDEN, and corresponding EC number in that cell (multiple)
                        else:
                            Title.append([RV,row[0],row[1],row[2],c_score, EC[0]]) #append according to RV number, Template, TM-score, IDEN, and corresponding EC number in that cell (only one)
                    else:
                        pass  
        except:
            pass #don't do anything if there is no RV in this dictionary

    for i in Title: 

        if i[0] in EC_dict.keys(): #This avoids the header and the RV number doesn't exist in the key
            #print EC_dict[i[0]]
            TASSER_EC = i[5].split(".") #split the I-TASSER produced EC number to 4 number to compare
            while len(TASSER_EC) <4: #Not all of them have 4 so avoid error append x in their position
                TASSER_EC.append('x')
            for j in range(len(EC_dict[i[0]])):
                manual_EC = EC_dict[i[0]][j].split(".") #Access to the manual EC number from the dictionary
                while len(manual_EC) <4:
                    manual_EC.append('-')
                if TASSER_EC[0] == manual_EC[0] and TASSER_EC[1] == manual_EC[1] and TASSER_EC[2] == manual_EC[2] and TASSER_EC[3] == manual_EC[3]: #Append 1, 1, 1, 1 if all match
                    i.append("1,1,1,1")
                elif TASSER_EC[0] == manual_EC[0] and TASSER_EC[1] == manual_EC[1] and TASSER_EC[2] == manual_EC[2] and TASSER_EC[3] != manual_EC[3]: #Append 1, 1, 1, 0 if 3 match and last one doesn't match, but if last one doesn't exist then don't match it
                    if TASSER_EC[3] =='x' or manual_EC[3] == '-':
                        i.append("1,1,1,NA")
                    else:
                        i.append("1,1,1,0")
                elif TASSER_EC[0] == manual_EC[0] and TASSER_EC[1] == manual_EC[1] and TASSER_EC[2] != manual_EC[2]: # Append 1,1,0, NA if 2 match and 3rd one not matching, but only 1, 1, NA, NA if first two match and third one doesn't exist
                    if TASSER_EC[2] == 'x' or manual_EC[2] == '-':
                        i.append("1,1,NA,NA")
                    else:
                        i.append("1,1,0,NA")
                elif TASSER_EC[0] == manual_EC[0] and TASSER_EC[1] != manual_EC[1]: #Append 1, 0, NA, NA if one match and second one not matching, but only 1, NA, NA, NA if second one doesn't exist
                    if TASSER_EC[1] =='x' or manual_EC[1] == '-':
                        i.append("1,NA,NA,NA")
                    else:
                        i.append("1,0,NA,NA")
                else:
                    i.append("0,NA,NA,NA")
            #The following loop will compare the line with 2 or more EC number comparison. The one with more 1 will be kept
            while len(i) >= 8:       
                Yescount1 = i[6].count("1")
                Yescount2 = i[7].count("1")
                if Yescount1 > Yescount2:
                    del i[7]
                else:
                    del i[6]
            i[6]=i[6].split(",") #split with .
            i[6]="\t".join(i[6]) #join the EC with tab
        if i[0] in EC_dict.keys(): #This avoids the header and the RV number doesn't exist in the key
            #print EC_dict[i[0]]
            TASSER_EC = i[5].split(".") #split the I-TASSER produced EC number to 4 number to compare
            while len(TASSER_EC) <4: #Not all of them have 4 so avoid error append x in their position
                TASSER_EC.append('x')
            for j in range(len(EC_dict[i[0]])):
                manual_EC = EC_dict[i[0]][j].split(".") #Access to the manual EC number from the dictionary
                while len(manual_EC) <4:
                    manual_EC.append('-')
                if TASSER_EC[0] == manual_EC[0]:
                    if TASSER_EC[1] =='x' or manual_EC[1] == '-':
                        i.append("1,NA,NA,NA")
                    elif TASSER_EC[1] == manual_EC[1]:
                        if TASSER_EC[2] =='x' or manual_EC[2] == '-':
                            i.append("1,1,NA,NA")
                        elif TASSER_EC[2] == manual_EC[2]:
                            if TASSER_EC[3] =='x' or manual_EC[3] == '-':
                                i.append("1,1,1,NA")
                            elif TASSER_EC[3] == manual_EC[3]:
                                i.append("1,1,1,1")
                            else:
                                i.append("1,1,1,0")
                        else:
                            i.append("1,1,0,0")
                    else:
                        i.append("1,0,0,0")
                else:
                    i.append("0,0,0,0")
            while len(i) >=9:
                Yescount3 = i[7].count("1")
                Yescount4 = i[8].count("1")
                if Yescount3 > Yescount4:
                    del i[8]
                else:
                    del i[7]
            i[7]=i[7].split(",") #split with .
            i[7]="\t".join(i[7]) #join the EC with tab
        print "\t".join(i) #join everything with tab

def C_score_EC_extraction(EC, RV_list):
    #print os.getcwd()
    EC_dict = EC(RV_list)
    GROUPHOME = os.environ['GROUPHOME']
    current_directory= os.getcwd() #Check the current directory
    
    result_directory = current_directory+"/results/" 
    Rv_list = open(RV_list, "U")
    #os.chdir(result_directory)  #Change directory to result directory
    #Rv_list.seek(0) #reshuffle the loop
    Title=[]
    Title.append(["RV number", "Template", "TM-Score", "IDEN" , "C-score", "E.C. Number","Position 1", "Position 2", "Position 3", "Position 4" , "Position 1 fix", "Position 2 fix", "Position 3 fix", "Position 4 fix"]) #header
                 
    for RV in Rv_list:
        RV= RV.strip() #get rid of blank space in case
        #name=RV+'_output.csv' #name of the each RV output file
        c_score_file = GROUPHOME+"/data/depot/hypotheticome/tuberculist/"+RV+"/model1/cofactor/ECsearchresult_model1.dat"
        try: #try to see if the following keyword exist in the EC dictionary
            report = open(c_score_file, "U")
            length_EC_dict =  len(EC_dict[RV]) #Check length of the value in the key that it loop through
            EC=[]
            for line in report:
                line = line.split(" ")
                EC = line[11].split(",")
                if len(EC)>1:
                    #The following statement will duplicate the I-TASSER row with multiple EC number
                    for i in range(len(EC)):
                        Title.append([RV, line[0], line[4], line[8], line[10], EC[i]])
                else:
                    Title.append([RV, line[0], line[4], line[8], line[10], EC[0]])

        except:
            pass #don't do anything if there is no RV in this dictionary

    for i in Title: 

        if i[0] in EC_dict.keys(): #This avoids the header and the RV number doesn't exist in the key
            #print EC_dict[i[0]]
            TASSER_EC = i[5].split(".") #split the I-TASSER produced EC number to 4 number to compare
            while len(TASSER_EC) <4: #Not all of them have 4 so avoid error append x in their position
                TASSER_EC.append('x')
            for j in range(len(EC_dict[i[0]])):
                manual_EC = EC_dict[i[0]][j].split(".") #Access to the manual EC number from the dictionary
                while len(manual_EC) <4:
                    manual_EC.append('-')
                if TASSER_EC[0] == manual_EC[0] and TASSER_EC[1] == manual_EC[1] and TASSER_EC[2] == manual_EC[2] and TASSER_EC[3] == manual_EC[3]: #Append 1, 1, 1, 1 if all match
                    i.append("1,1,1,1")
                elif TASSER_EC[0] == manual_EC[0] and TASSER_EC[1] == manual_EC[1] and TASSER_EC[2] == manual_EC[2] and TASSER_EC[3] != manual_EC[3]: #Append 1, 1, 1, 0 if 3 match and last one doesn't match, but if last one doesn't exist then don't match it
                    if TASSER_EC[3] =='x' or manual_EC[3] == '-':
                        i.append("1,1,1,NA")
                    else:
                        i.append("1,1,1,0")
                elif TASSER_EC[0] == manual_EC[0] and TASSER_EC[1] == manual_EC[1] and TASSER_EC[2] != manual_EC[2]: # Append 1,1,0, NA if 2 match and 3rd one not matching, but only 1, 1, NA, NA if first two match and third one doesn't exist
                    if TASSER_EC[2] == 'x' or manual_EC[2] == '-':
                        i.append("1,1,NA,NA")
                    else:
                        i.append("1,1,0,NA")
                elif TASSER_EC[0] == manual_EC[0] and TASSER_EC[1] != manual_EC[1]: #Append 1, 0, NA, NA if one match and second one not matching, but only 1, NA, NA, NA if second one doesn't exist
                    if TASSER_EC[1] =='x' or manual_EC[1] == '-':
                        i.append("1,NA,NA,NA")
                    else:
                        i.append("1,0,NA,NA")
                else:
                    i.append("0,NA,NA,NA")
            #The following loop will compare the line with 2 or more EC number comparison. The one with more 1 will be kept
            while len(i) >= 8:       
                Yescount1 = i[6].count("1")
                Yescount2 = i[7].count("1")
                if Yescount1 > Yescount2:
                    del i[7]
                else:
                    del i[6]
            i[6]=i[6].split(",") #split with .
            i[6]="\t".join(i[6]) #join the EC with tab
        if i[0] in EC_dict.keys(): #This avoids the header and the RV number doesn't exist in the key
            #print EC_dict[i[0]]
            TASSER_EC = i[5].split(".") #split the I-TASSER produced EC number to 4 number to compare
            while len(TASSER_EC) <4: #Not all of them have 4 so avoid error append x in their position
                TASSER_EC.append('x')
            for j in range(len(EC_dict[i[0]])):
                manual_EC = EC_dict[i[0]][j].split(".") #Access to the manual EC number from the dictionary
                while len(manual_EC) <4:
                    manual_EC.append('-')
                if TASSER_EC[0] == manual_EC[0]:
                    if TASSER_EC[1] =='x' or manual_EC[1] == '-':
                        i.append("1,NA,NA,NA")
                    elif TASSER_EC[1] == manual_EC[1]:
                        if TASSER_EC[2] =='x' or manual_EC[2] == '-':
                            i.append("1,1,NA,NA")
                        elif TASSER_EC[2] == manual_EC[2]:
                            if TASSER_EC[3] =='x' or manual_EC[3] == '-':
                                i.append("1,1,1,NA")
                            elif TASSER_EC[3] == manual_EC[3]:
                                i.append("1,1,1,1")
                            else:
                                i.append("1,1,1,0")
                        else:
                            i.append("1,1,0,0")
                    else:
                        i.append("1,0,0,0")
                else:
                    i.append("0,0,0,0")
            while len(i) >=9:
                Yescount3 = i[7].count("1")
                Yescount4 = i[8].count("1")
                if Yescount3 > Yescount4:
                    del i[8]
                else:
                    del i[7]
            i[7]=i[7].split(",") #split with .
            i[7]="\t".join(i[7]) #join the EC with tab
        print "\t".join(i) #join everything with tab

def main(RV_list):
    current_directory= os.getcwd() #Check the current directory
    TM_score_EC_extraction(EC, RV_list)
    os.chdir(current_directory)
    C_score_EC_extraction(EC, RV_list)

    
if __name__ == "__main__":
        main(sys.argv[1])
    
