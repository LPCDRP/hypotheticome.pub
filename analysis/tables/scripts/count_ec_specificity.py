#!usr/bin/env python
# AUTHOR: Deepika Gunasekaran
# NOTE: All paths in this file are hard-coded
# Run this script in /hypotheticome.pub/analysis/tables/scripts

def standardizeECs(list_of_ecs):
	'''
	This function takes as input a list of EC numbers and changes it to a standard format. Ex: #.#.#.- instead of #.#.#
	'''
	standardized_list = []
	for i in list_of_ecs:
		i = i.strip()
		count_dots = i.count('.')
		if count_dots == 3:
			standardized_list.append(i)
			continue
		elif count_dots == 2:
			i = i + '.-'
			standardized_list.append(i)
			continue
		elif count_dots == 1:
			i = i + '.-.-'
                        standardized_list.append(i)
			continue
		else:
			i = i + '.-.-.-'
                        standardized_list.append(i)
			continue
	return standardized_list


def countECs(list_of_ecs):
	'''
	This function takes as input a list of EC numbers, counts the number of '-'s in the EC and maintains a dictionary with the level of specificity
	'''
	ec_spec_dict = {4: 0, 3: 0, 2: 0, 1:0}
	for i in list_of_ecs:
		i = i.strip()
		count_spec = 4 - i.count('-')
		ec_spec_dict[count_spec] += 1
	return ec_spec_dict


uniprot_ecs = open('../raw_data/uniprot_ecs').readlines()
kegg_ecs = open('../raw_data/kegg_ecs').readlines()
biocyc_ecs = open('../raw_data/biocyc_ecs').readlines()
tuberculist_ecs = open('../raw_data/tuberculist_ecs').readlines()

hypotheticome_ecs = open('../raw_data/hypotheticome_ecs').readlines()
uniprot_hyp_ecs = open('../raw_data/uniprot_hyp_ecs').readlines()
kegg_hyp_ecs = open('../raw_data/kegg_hyp_ecs').readlines()
biocyc_hyp_ecs = open('../raw_data/biocyc_hyp_ecs').readlines()
tuberculist_hyp_ecs = open('../raw_data/tuberculist_hyp_ecs').readlines()

uniprot = countECs(uniprot_ecs)
kegg = countECs(kegg_ecs)
biocyc_mod_ecs = standardizeECs(biocyc_ecs)
biocyc = countECs(biocyc_mod_ecs)
tuberculist = countECs(tuberculist_ecs)

hyp = countECs(hypotheticome_ecs)
uniprot_hyp = countECs(uniprot_hyp_ecs)
kegg_hyp = countECs(kegg_hyp_ecs)
biocyc_hyp = countECs(biocyc_hyp_ecs)
tuberculist_hyp = countECs(tuberculist_hyp_ecs)


# Writing output
all_genes = open('../dbs_ec_specificity.tsv', 'w')
header = 'Database\tSpecificity-4\tSpecificity-3\tSpecificity-2\tSpecificity-1\tTotal\n'
all_genes.write(header)
uniprot_all_genes = 'UniProt\t' + str(uniprot[4]) + '\t' + str(uniprot[3]) + '\t' + str(uniprot[2]) + '\t' + str(uniprot[1]) + '\t' + str(sum(uniprot.values())) + '\n'
all_genes.write(uniprot_all_genes)
kegg_all_genes = 'KEGG\t' + str(kegg[4]) + '\t' + str(kegg[3]) + '\t' + str(kegg[2]) + '\t' + str(kegg[1]) + '\t' + str(sum(kegg.values())) + '\n'
all_genes.write(kegg_all_genes)
biocyc_all_genes = 'BioCyc\t' + str(biocyc[4]) + '\t' + str(biocyc[3]) + '\t' + str(biocyc[2]) + '\t' + str(biocyc[1]) + '\t' + str(sum(biocyc.values())) + '\n'
all_genes.write(biocyc_all_genes)
tuberculist_all_genes = 'TubercuList\t' + str(tuberculist[4]) + '\t' + str(tuberculist[3]) + '\t' + str(tuberculist[2]) + '\t' + str(tuberculist[1]) + '\t' + str(sum(tuberculist.values())) + '\n'
all_genes.write(tuberculist_all_genes)
all_genes.close()

hyp_genes = open('../hypotheticome_genes_ec_specificity.tsv', 'w')
header = 'Database\tSpecificity-4\tSpecificity-3\tSpecificity-2\tSpecificity-1\tTotal\n'
hyp_genes.write(header)
hypotheticome_genes = 'Hypotheticome\t' + str(hyp[4]) + '\t' + str(hyp[3]) + '\t' + str(hyp[2]) + '\t' + str(hyp[1]) + '\t' + str(sum(hyp.values())) + '\n'
hyp_genes.write(hypotheticome_genes)
uniprot_hyp_genes = 'UniProt\t' + str(uniprot_hyp[4]) + '\t' + str(uniprot_hyp[3]) + '\t' + str(uniprot_hyp[2]) + '\t' + str(uniprot_hyp[1]) + '\t' + str(sum(uniprot_hyp.values())) + '\n'
hyp_genes.write(uniprot_hyp_genes)
kegg_hyp_genes = 'KEGG\t' + str(kegg_hyp[4]) + '\t' + str(kegg_hyp[3]) + '\t' + str(kegg_hyp[2]) + '\t' + str(kegg_hyp[1]) + '\t' + str(sum(kegg_hyp.values())) + '\n'
hyp_genes.write(kegg_hyp_genes)
biocyc_hyp_genes = 'BioCyc\t' + str(biocyc_hyp[4]) + '\t' + str(biocyc_hyp[3]) + '\t' + str(biocyc_hyp[2]) + '\t' + str(biocyc_hyp[1]) + '\t' + str(sum(biocyc_hyp.values())) + '\n'
hyp_genes.write(biocyc_hyp_genes)
tuberculist_hyp_genes = 'TubercuList\t' + str(tuberculist_hyp[4]) + '\t' + str(tuberculist_hyp[3]) + '\t' + str(tuberculist_hyp[2]) + '\t' + str(tuberculist_hyp[1]) + '\t' + str(sum(tuberculist_hyp.values())) + '\n'
hyp_genes.write(tuberculist_hyp_genes)
hyp_genes.close()


print('UniProt')
print(uniprot)
print('KEGG')
print(kegg)
print('BioCyc')
#print(len(biocyc_ecs))
#print(len(biocyc_mod_ecs))
print(biocyc)
print('TubercuList')
print(tuberculist)
print('Hypotheticome')
print(hyp)
print('UniProt-Hypotheticome')
print(uniprot_hyp)
print('KEGG-Hypotheticome')
print(kegg_hyp)
print('BioCyc-Hypotheticome')
print(biocyc_hyp)
print('TubercuList-Hypotheticome')
print(tuberculist_hyp)

