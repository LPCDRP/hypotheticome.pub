#!/usr/bin/env python
import urllib2
import sys

fout = sys.stdout
mannotated = sys.stdin.readlines()
#fout = open('mtbsysportalncbi.tsv', 'w')
#fin = open('/home/azlotnic/hypotheticome/hypotheticome/analysis/complete-updates/just-mannotation-genes.csv', 'U')
#mannotated = fin.readlines()
#fout.write("gene\tmtbsystemsportal\tEntrez Protein\n") 
fout.writelines("gene\tmtbsystemsportal\tEntrez Protein\n") 


for x in range(1, len(mannotated)):
    url = 'http://networks.systemsbiology.net/mtb/genes/' + mannotated[x]
    response = urllib2.urlopen(url)
    page = response.read(200000)
    productstr = page[page.find('title>'):page.find('/title')]
    product = productstr[productstr.find(mannotated[x].strip()):productstr.find('|')]
    product = product.replace(mannotated[x].strip(), "\t") 
    try:
        ncbistr = page[page.find('http://www.ncbi.nlm.nih.gov/protein'):page.find('Entrez Protein')]
        ncbiurl = ncbistr[0:ncbistr.find("\"")]
        #print ncbiurl
        try:
            response = urllib2.urlopen(ncbiurl)
            page = response.read(200000)
            ncbistr = page[page.find('title>'):page.find('/title')]
            ncbi = ncbistr[6:ncbistr.find("[")]
        except ValueError:
            ncbi = ""
    except AttributeError:
        ncbi = ""
    fout.writelines(mannotated[x].strip() + product + '\t' + ncbi + '\n')
fout.close()
