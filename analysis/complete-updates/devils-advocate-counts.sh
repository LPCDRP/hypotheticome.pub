#!/bin/bash

printf "\ngenes annotated in our literature curation effort that other databases do not have a descriptive annotation for (annotation same as TubercuList\n"
grep -i "#FFFF00" devils-advocate.html | wc -l
printf "\ngenes annotated in our literature curation effort that have a more specific annotation than any other database\n"
grep -i "#FF00FF" devils-advocate.html | wc -l
printf "\noverall genes with stronger annotations from our literature curation effort than from any other database\n"
grep -i -e "#FF00FF" -e "#FFFF00" devils-advocate.html | wc -l
printf "\ngenes annotated in our literature curation effort that other databases do not have a descriptive annotation for (annotation same as TubercuList, EXCLUDING ANTIGENS\n"
grep -i "#FFFF00" devils-advocate.html | grep -iv -e immuno -e antigen | wc -l
printf "\ngenes annotated in our literature curation effort that have a more specific annotation than any other database, EXCLUDING ANTIGENS\n"
grep -i "#FF00FF" devils-advocate.html | grep -iv -e immuno -e antigen | wc -l
printf "\noverall genes with stronger annotations from our literature curation effort than from any other database, EXCLUDING ANTIGENS\n"
grep -i -e "#FF00FF" -e "#FFFF00" devils-advocate.html | grep -iv -e antigen -e immuno | wc -l
printf "\ngenes charactered by our manual curation effort that provided additional strong product annotation to the product annotation already present\n"
grep -i "#00FFFF" devils-advocate.html | wc -l
printf "\ngenes charactered by our manual curation effort that provided additional strong product annotation to the product annotation already present, EXCLUDING ANTIGENS\n"
grep -i "#00FFFF" devils-advocate.html | grep -iv antigen | wc -l
printf "\noverall genes with annotations from our literature curation that other databases did not previously have\n"
grep -i -e "#FF00FF" -e "#FFFF00" -e "#00FFFF" devils-advocate.html | wc -l
printf "\noverall genes with annotations from our literature curation that other databases did not previously have, EXCLUDING ANTIGENS\n"
grep -i -e "#FF00FF" -e "#FFFF00" -e "#00FFFF" devils-advocate.html | grep -iv -e antigen -e immuno | wc -l

