#!/bin/bash
#Product Rv's are in all-products.tsv
#Product Rv's and names of products are in all-products-with-names.tsv
#Note Rv's are in all-notes-just-Rvs.txt
#PMIDs and DOIs that we cited in all the papers are in all-pmids-dois.txt
#"Probable\" and \"Putative\" products are subcategories of the total number of products reported and should not be added to the printed \"products\" total
#"Genes with notes\" are number of genes with at least 1 note from both the hypothetical and ambigous gene sets \n"
#
#
printf "\nProduct Rv's are in all-products.tsv\nProduct Rv's and names of products are in all-products-with-names.tsv\nProduct Rv's and names of probable products are in probable-products-with-names.tsv\nProduct Rv's and names of putative products are in putative-products-with-names.tsv\nProduct Rv's and names of definite products are in definite-products-with-names.tsv\nNote Rv's are in all-notes-just-Rvs.txt\nPMIDs and DOIs that we cited in all the papers are in all-pmids-dois.txt\n\n\"Probable\" and \"Putative\" products are subcategories of the total number of products reported and should not be added to the printed \"products\" total\n\n\"Genes with notes\" are number of genes with at least 1 note from both the hypothetical and ambigous gene sets \n"
prob=''
put=''
all=''
def=''
notes=''

git -C $ANNPATH/features diff --name-only 59362953 | cut -d/ -f2 | cut -d. -f1 | sort -u > $HYPDIR/changed-genes.txt
grep -iv -e unknown -e hypothetical -e "conserved protein" -e "conserved" -e "surface-exposed" -e uncharacterized -e "inducible gene" -e "cell membrane protein" $HYPDIR/manual-annotations.tsv > almost-all-products-with-names.tsv
grep -e "conserved" -e " transmembrane"  -e "membrane" -e uncharacterized $HYPDIR/manual-annotations.tsv | grep -e "/" > additional-products.tsv
cat almost-all-products-with-names.tsv additional-products.tsv | sort -u > all-products-with-names.tsv
comm -1 -2 changed-genes.txt <(cut -f1 all-products-with-names.tsv) > all-products.tsv 
comm -2 -3 changed-genes.txt <(cut -f1 all-products-with-names.tsv) > all-notes.txt
rm almost-all-products-with-names.tsv additional-products.tsv changed-genes.txt
all=$(wc -l all-products.tsv)
printf "\nproducts: "$all

while read p; do
    printf "\n"$p"\n";
    grep -i -m 1 -e "probabl" $ANNPATH/features/$p.tbl
done<all-products.tsv > probable-products-with-names.tsv
while read p; do
    printf "\n"$p"\n";
    grep -i -m 1 -e "putative" $ANNPATH/features/$p.tbl
done<all-products.tsv > putative-products-with-names.tsv
while read p; do
    printf "\n"$p"\n";
    grep -vi -e "putative" -e "probabl" -e "potential" -e "possible" $ANNPATH/features/$p.tbl  | grep "product" 
done<all-products.tsv > definite-products-with-names.tsv


prob=$(while read p; do
    grep -io -m 1 -e "probabl" $ANNPATH/features/$p.tbl
done<all-products.tsv | wc -l)

printf "\nprobable: "$prob
put=$(while read p; do
    grep -io -m 1 -e "putative" $ANNPATH/features/$p.tbl
done<all-products.tsv | wc -l)
printf "\nputative: "$put

enzymes=$(grep -i "ase" all-products-with-names.tsv | wc -l)
printf "\nenzymes: "$enzymes

probenzymes=$(grep -i "ase" all-products-with-names.tsv | grep -i "probable" | wc -l)
printf "\nprobable enzymes: "$probenzymes

putenzymes=$(grep -i "ase" all-products-with-names.tsv | grep -i "putative" | wc -l)
printf "\nputative enzymes: "$putenzymes

antigens=$(grep -i "antigen" all-products-with-names.tsv | wc -l)
printf "\nantigens: "$antigens

probantigens=$(grep -i "antigen" all-products-with-names.tsv | grep -i "probable" | wc -l)
printf "\nprobable antigens: "$probantigens

putantigens=$(grep -i "antigen" all-products-with-names.tsv | grep -i "putative" | wc -l)
printf "\nputative antigens: "$putantigens

regulators=$(grep -i -e "regulator" -e "repress" -e "inhibit" -e "activat" -e "induc" all-products-with-names.tsv | wc -l)
printf "\nregulatory proteins/regulators: "$regulators

probregulators=$(grep -i -e "regulator" -e "repress" -e "inhibit" -e "activat" -e "induc" all-products-with-names.tsv | grep -i "probable" | wc -l)
printf "\nprobable regulatory proteins/regulators: "$probregulators
 
putregulators=$(grep -i -e "regulator" -e "repress" -e "inhibit" -e "activat" -e "induc" all-products-with-names.tsv | grep -i "putative" | wc -l)
printf "\nputative regulatory proteins/regulators: "$putregulators

binding=$(grep -i "binding protein" all-products-with-names.tsv | wc -l)
printf "\nbinding proteins: "$binding

probbinding=$(grep -i "binding protein" all-products-with-names.tsv | grep -i "probable" | wc -l)
printf "\nprobable binding proteins: "$probbinding

putbinding=$(grep -i "binding protein" all-products-with-names.tsv | grep -i "putative" | wc -l)
printf "\nputative binding proteins: "$putbinding

others=$(grep -iv -e "binding protein" -e "ase" -e "antigen" -e "regulator" -e "repress" -e "inhibit" -e "activat" -e "induc" all-products-with-names.tsv | wc -l)
printf "\nother characterized products: "$others

probothers=$(grep -iv -e "binding protein" -e "ase" -e "antigen" -e "regulator" -e "repress" -e "inhibit" -e "activat" -e "induc" all-products-with-names.tsv | grep -i "probable" | wc -l)
printf "\nprobable other characterized products: "$probothers

putothers=$(grep -iv -e "binding protein" -e "ase" -e "antigen" -e "regulator" -e "repress" -e "inhibit" -e "activat" -e "induc" all-products-with-names.tsv | grep -i "putative" | wc -l)
printf "\nputative other characterized products: "$putothers


#def="$(($all-$put))"
#all="$(($def-$prob))"

#printf "\ndefinite: " $def

printf "\n\ntotal number of genes with notes: " 
(while read p; do
    grep -v -e "ncnote" -ie FunctionalCategory: $ANNPATH/features/$p.tbl;
 done < all-notes.txt) | grep -e "note" -e "Rv" | ./just-notes.py | grep -Eo "Rv[0-9]{4}c*" > all-notes-just-Rvs.txt
notes=$(wc -l all-notes-just-Rvs.txt)
printf $notes
#(while read p; do
#    grep -v -e "ncnote" -ie FunctionalCategory: $ANNPATH/features/$p.tbl;
#done < all-notes.txt) | awk '{print $1}'| uniq | grep -e note | wc -l
printf "\n\ntotal number of note annotations (multiple per gene): " 
(while read p; do
    printf $p
    grep -v -e "ncnote" -ie FunctionalCategory: $ANNPATH/features/$p.tbl;
done < all-notes.txt) | grep -e note | wc -l
printf "\n\ntotal number of papers cited: " 
grep -Eo -e "PMID:[0-9]{8}" -e "PMID: [0-9]{8}" -e "doi: [0-9.//a-z/-]+" $ANNPATH/features/*.tbl | cut -f3 -d: | sed 's/ //g' | sort -u > all-pmids-dois.txt
pmids=$(wc -l all-pmids-dois.txt)
printf $pmids
printf "\n"
