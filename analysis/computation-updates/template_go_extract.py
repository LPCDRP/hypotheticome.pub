import csv
import glob

def template_extract():
    csv_file = glob.glob("../computation-updates/template_GO_term/*.csv")
    print '\t'.join(['Name', 'Template', 'TM-Score', 'IDEN', 'E.C. Number', 'Geometric Mean', 'Property', 'GO Term', 'Function Name'])
    for i in csv_file:
        name =i.split('/')[-1].split('_')[0]
        with open(i, 'rb') as csvfile:
            reader = csv.reader(csvfile, delimiter='\t',quotechar='|',quoting=csv.QUOTE_MINIMAL) #able to read csv
            next(reader)
            for line in reader:
                if float(line[1])>0.5:
                    line.insert(0, name)
                    print '\t'.join(line)

if __name__ == '__main__':
    template_extract()
