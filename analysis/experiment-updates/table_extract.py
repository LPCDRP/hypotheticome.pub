import commands
import os
import sys
from xml.dom import minidom
from multiprocessing import Pool
import math
import re


def extraction(RV_list, f2="try.csv"):
        GROUPHOME=os.environ['GROUPHOME']
        current_directory= os.getcwd() #Check the current directory
        result_directory = current_directory+"/results/"
        print result_directory
        if not os.path.exists(result_directory): #Check if you have result folder in this directory, if not, then create one
                os.makedirs(result_directory)
        Rv_list = open(RV_list, "U")
        for RV in Rv_list:
                RV= RV.strip() #get rid of blank space in case
                template_dict = {}
                template = ""
                TM_score = 0
                SID = 0
                template_length = 0
                new_template= ""
                Title = "Template" + "\t" + "TM-Score" + "\t" + "IDEN" + "\t" + "E.C. Number" + '\n'
                input_path = GROUPHOME+"/data/depot/hypotheticome/tuberculist/"+RV+"/model1/cofactor/PDBsearchresult_ext_model1.dat"
                ref_input_path = GROUPHOME+"/data/depot/hypotheticome/reftest/"+RV+"/model1/cofactor/PDBsearchresult_ext_model1.dat"
                if os.path.isfile(input_path):
                        report = open(input_path, "U")
                elif os.path.isfile(ref_input_path):
                        report = open(ref_input_path, "U")
                else:
                        print "No file for " + RV
                for line in report:
                        EC_number = "" #EC number, can be multiple
                        parameter =line.split(',') # ['Template=3c1zA     ', ' Query=model1.pdb', ' Lch_t= 349', ' Lch_q= 358', ' TM-score_q= 0.9504', ' TM-score_t= 0.9746', ' ALN= 346', ' SID= 0.379\n']
                        template = parameter[0][9:] # Gives the template
                        template = template.strip()
                        TM_score = float(parameter[4][13:]) #Gives TM_score
                        SID = float(parameter[7][6:]) #Gives SID aka IDEN value 
                        template_dict[template] = [TM_score, SID] #Create dictionary with key of Template and value of TM_score and SID
                        template_length = len(template) #know the template length so you can add a dot in next line
                        new_template = template[0:template_length-1] + "." + template[template_length-1] #Refer last line
                        XML = commands.getoutput("curl -s http://www.rcsb.org/pdb/rest/describeMol?structureId=" + new_template +" >intermediate.xml") #This will tell terminal to look for the current looping template xml file
                        xmldoc = minidom.parse("intermediate.xml") #parse the xml file
                        EClist = xmldoc.getElementsByTagName('enzClass')#List of the EC number in this template
                        number_EC =  len(EClist)#how many EC number in the EC list
                        EC=[] #This is going to be the EC of the template
                        geom_mean = math.sqrt(TM_score * SID)
                        for i in range(number_EC):
                                EC.append(EClist[i].attributes['ec'].value) #Append all the EC number to this EC list
                        if EC:    
                                for j in range(number_EC):
                                        if j == 0:
                                                EC_number += EC[j]
                                        else:
                                                EC_number += "/" + EC[j]
                        if TM_score > 0.0:
                                Title += "\t".join([template, str(TM_score), str(SID), EC_number, str(geom_mean)]) + "\n"
                output = RV+"_output.csv"
                f2=output
                print output + " result successfully created"
                fout = open(result_directory+output, 'w')
                fout.write(Title)
                fout.close()
                

if __name__ == "__main__":
        P = Pool()
        P.map(extraction, [sys.argv[1]])
