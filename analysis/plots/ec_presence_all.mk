SHELL := /bin/bash 

.DELETE_ON_ERROR:
.SECONDARY:

HYP_SET := ../../res/tblist-hypotheticome.tsv
TUBERCULIST_ALL := ../../res/tuberculist-EC-go-all-tblist-genes.tsv
MANNOTATION := ../../../Mtb-H37Rv-annotation/features
RESOURCES := ../../res

# DEPENDENCIE: R package: VennDiagram

#.PHONY : clean

temp_files :  temp/hyp_list temp/mannotation_with_ec temp/iTASSER_with_ec temp/hypotheticome_with_ec temp/uniprot_all_ec temp/uniprot_hyp_ec temp/kegg_all_ec temp/kegg_hyp_ec temp/biocyc_all_ec \
	      temp/biocyc_hyp_ec temp/tblist_all_ec temp/tblist_hyp_ec plot_mannotation rename_mannotation plot rename clean
scripts : scripts/ecPresence.R scripts/process_iTASSER_EC.py

# Get all the genes in the hypotheticome list
temp/hyp_list: $(HYP_SET)
	mkdir -p temp
	cut -f1 $(HYP_SET) | sort | uniq > $@ || touch $@

temp/mannotation_with_ec: $(MANNOTATION)
	grep 'EC number' $(MANNOTATION)/*.tbl | grep -v 'ncnote' | cut -f1 | cut -d '/' -f6 | grep 'Rv' | cut -d '.' -f1 | sort | uniq > $@ || touch $@

temp/iTASSER_with_ec: $(MANNOTATION)
	grep -B2 'EC_number' $(MANNOTATION)/EC-GO-reconciled.tbl | grep 'locus_tag' | cut -f6 | sort | uniq > $@ || touch $@

temp/hypotheticome_with_ec: $(temp_files)
	cat temp/mannotation_with_ec temp/iTASSER_with_ec | sort | uniq > $@ || touch $@

temp/uniprot_all_ec: $(RESOURCES)
	grep '(EC' $(RESOURCES)/uniprot.tsv | cut -f1 | grep -v '/' | sort | uniq > $@
	cat <(grep '(EC' $(RESOURCES)/uniprot.tsv | cut -f1 | grep '/' | tr / '\n') >> $@ || touch $@

temp/uniprot_hyp_ec: $(RESOURCES) $(temp_files)
	comm -1 -2 temp/hyp_list <(cat temp/uniprot_all_ec | sort | uniq) > $@ || touch $@

temp/kegg_all_ec: $(RESOURCES)
	awk '$$NF !~ /NA/ { print $$1 }' $(RESOURCES)/kegg_annotations > $@ || touch $@

temp/kegg_hyp_ec: $(RESOURCES) $(temp_files)
	comm -1 -2 temp/hyp_list <(cat temp/kegg_all_ec | sort | uniq) > $@ || touch $@

temp/biocyc_all_ec: $(RESOURCES)
	awk '$$NF ~ /EC/ { print $$2 }' $(RESOURCES)/BioCyc_annotations.txt| grep 'Rv' | cut -d'"' -f2 | sort | uniq > $@ || touch $@

temp/biocyc_hyp_ec: $(RESOURCES) $(temp_files)
	comm -1 -2 temp/hyp_list <(cat temp/biocyc_all_ec | sort | uniq) > $@ || touch $@

temp/tblist_all_ec: $(RESOURCES)
	awk '$$2 ~ /^[0-9]/ { print $$1 }' $(RESOURCES)/tuberculist-EC-go-all-tblist-genes.tsv | grep -v '/' | sort | uniq > $@ || touch $@
	cat <(awk '$$2 !~ /./ { print $$1 }' $(RESOURCES)/tuberculist-EC-go-all-tblist-genes.tsv | grep '/' | tr / '\n') >> $@ || touch $@

temp/tblist_hyp_ec: $(RESOURCES) $(temp_files)
	comm -1 -2 temp/hyp_list <(cat temp/tblist_all_ec | sort | uniq) > $@ || touch $@

plot_mannotation: $(scripts) $(temp_files)
	Rscript scripts/ecPresenceMannotation.R 

rename_mannotation:
	mv Rplots.pdf ec_presence_man_genes.pdf

plot: $(scripts) $(temp_files)
	Rscript scripts/ecPresence.R 

rename:
	mv Rplots.pdf ec_presence_hyp_genes.pdf

clean:
	rm -r temp/

