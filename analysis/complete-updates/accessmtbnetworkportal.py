#!/usr/bin/env python
import urllib2
import sys
import re

fout = sys.stdout
mannotated = sys.stdin.readlines()
#fout = open('mtbsysportalgoterms.tsv', 'w')
#fin = open('/home/azlotnic/hypotheticome/hypotheticome/res/mtb-genes-entire-genome.tsv ', 'U')
#mannotated = fin.readlines()
gostartregex = re.compile('h3><a href=\"/mtb/Go-Terms/')
goendregex = re.compile('</a></h3')
#fout.write("gene\tEC Number\tGO Terms") 
fout.writelines("gene\tproduct\tEC Number\tGO Terms") 
for x in range(0, len(mannotated)):
    url = 'http://networks.systemsbiology.net/mtb/genes/' + mannotated[x]
    response = urllib2.urlopen(url)
    page = response.read(200000)
    productstr = page[page.find('title>'):page.find('/title')] 
    product = productstr[productstr.find(mannotated[x].strip()):productstr.find('|')]
    product = product.replace(mannotated[x].strip(), "\t") 
    gostart = []
    goend = []
    gotermname = []
    gostart = [m.end() for m in re.finditer(gostartregex, page)]
    goend = [n.start() for n in re.finditer(goendregex, page)]
    for i in range (0, len(gostart)):
        gotermname.append(page[(gostart[i]+11):goend[i]])
    if "(EC" not in productstr:
        fout.writelines("\n" + mannotated[x].strip() + '\t' + product)
        fout.writelines('\t\t')
    else:
        fout.writelines("\n" + mannotated[x].strip() + '\t' + product[0:product.find('(')])
        fout.writelines('\t' + productstr[(productstr.find("(EC")+1):productstr.find(')')])
    for j in gotermname: 
        fout.writelines('\t' + j)
fout.close()
